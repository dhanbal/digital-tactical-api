package sampletest;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import com.intuit.karate.Results;
import com.intuit.karate.Runner;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;


import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;

public class TestRunnerParallel {
    @Test
    public void testParallel() {
        System.setProperty("karate.env", "SQA");
        Results results = Runner.path("classpath:sampletest/features").tags("@cirptv1.1_meijer-known").parallel(1);
        //generateReport(results.getReportDir()); cirptv_meijer-known
        System.out.println("results.getFailCount() "+results.getFailCount() );
        assertTrue(results.getErrorMessages(), results.getFailCount() == 0);
    }
    public static void generateReport(String karateOutputPath) {
        Collection<File> jsonFiles = FileUtils.listFiles(new File(karateOutputPath), new String[] {"json"}, true);
        List<String> jsonPaths = new ArrayList(jsonFiles.size());
        jsonFiles.forEach(file -> jsonPaths.add(file.getAbsolutePath()));
        Configuration config = new Configuration(new File("target"), "demo");
        ReportBuilder reportBuilder = new ReportBuilder(jsonPaths, config);
        reportBuilder.generateReports();
    }
}
