package reusable;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvException;
import com.redis.S;
import io.gatling.commons.stats.assertion.In;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class WriteToCSVFile {

    public void writeToWallet(String filename, String promotions) throws IOException, CsvException {

        String classpathStr = System.getProperty("user.dir");
        String csvFile = classpathStr + "\\src\\test\\java\\sampletest\\dataFiles\\" + filename;
//        System.out.println(csvFile);
        File inputFile = new File(csvFile);

        // Read existing file
        CSVReader reader = new CSVReader(new FileReader(inputFile));
        List<String[]> csvBody = reader.readAll();

        Iterator<String[]> itr = csvBody.iterator();
        int row = 0;
        while(itr.hasNext()){
            if(row == 0) {
                row++;
                itr.next();
                continue;
            }
            // get CSV row column  and replace with by using row and column
            csvBody.get(row)[2] = promotions;
            row++;
            itr.next();
        }
        reader.close();

        // Write to CSV file which is open
        CSVWriter writer = new CSVWriter(new FileWriter(inputFile), CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END);
        writer.writeAll(csvBody);
        writer.flush();
        writer.close();
    }


    public void writeToWalletShoplifter(String id,String filename, String promotions, Object spon1, Object spon2) throws IOException, CsvException {

        String classpathStr = System.getProperty("user.dir");
        String csvFile = classpathStr + "\\src\\test\\java\\sampletest\\dataFiles\\" + filename;
        File inputFile = new File(csvFile);
        // Read existing file
        CSVReader reader = new CSVReader(new FileReader(inputFile));
        List<String[]> csvBody = reader.readAll();
        String sponsoredTop="";
        Iterator<String[]> itr = csvBody.iterator();
        int row = 0;
        while(itr.hasNext()){
            if(row == 0) {
                row++;
                itr.next();
                continue;
            }
            // get CSV row column  and replace with by using row and column
            csvBody.get(row)[2] = promotions;
            csvBody.get(row)[0] = id;
            //add the sponsored rank
                String[] rank=csvBody.get(row)[3].split(";");

                if(spon1 instanceof Integer && spon2 instanceof Integer) {
                    int sponsored1 = (int) spon1;
                    int sponsored2 = (int) spon2;
                    for (int position = 0; position < rank.length; position++) {
                        if ((int) Float.parseFloat(rank[position]) == sponsored1 || (int) Float.parseFloat(rank[position]) == sponsored2) {
                            if (position == rank.length) {
                                sponsoredTop = sponsoredTop + "1";
                            } else {
                                sponsoredTop = sponsoredTop + "1;";
                            }
                        } else {
                            if (position == rank.length - 1) {
                                sponsoredTop = sponsoredTop + "0";
                            } else {
                                sponsoredTop = sponsoredTop + "0;";
                            }

                        }
                    }
                    csvBody.get(row)[4] = sponsoredTop;
                }
            if( spon1 instanceof Integer && !(spon2 instanceof Integer)){
                {
                    int flagCounter=0;
                    int sponsored1 = (int) spon1;
                    String sponsored2 = (String) spon2;
                    for (int position = 0; position < rank.length; position++) {
                        if ((int) Float.parseFloat(rank[position]) == sponsored1 ) {
                            if (position == rank.length) {
                                sponsoredTop = sponsoredTop + "1";
                            } else {
                                sponsoredTop = sponsoredTop + "1;";
                            }
                        } else {
                            if (position == rank.length - 1) {
                                sponsoredTop = sponsoredTop + "0";
                            } else {
                                if(position !=sponsored1 && flagCounter==0 ){
                                    sponsoredTop = sponsoredTop +sponsored2+";";
                                    flagCounter++;
                                }else {
                                    sponsoredTop = sponsoredTop + "0;";
                                }
                            }

                        }
                    }
                    csvBody.get(row)[4] = sponsoredTop;
                }

            }
            if( !(spon1 instanceof Integer) && (spon2 instanceof Integer)){
                {
                    int flagCounter=0;
                    String sponsored1 = (String) spon1;
                    int sponsored2 = (int) spon2;
                    for (int position = 0; position < rank.length; position++) {
                        if ((int) Float.parseFloat(rank[position]) == sponsored2) {
                            if (position == rank.length) {
                                sponsoredTop = sponsoredTop + "1";

                            } else {
                                sponsoredTop = sponsoredTop + "1;";
                                flagCounter++;
                            }
                        } else {


                            if (position == rank.length - 1) {
                                sponsoredTop = sponsoredTop + "0";
                            } else {
                                if(position !=sponsored2 && flagCounter==0 ){
                                    sponsoredTop = sponsoredTop +sponsored1+";";
                                    flagCounter++;
                                }else {
                                    sponsoredTop = sponsoredTop + "0;";
                                }
                                }

                        }
                    }
                    csvBody.get(row)[4] = sponsoredTop;
                }

            }
            if( !(spon1 instanceof Integer) && !(spon2 instanceof Integer)){
                {
                     sponsoredTop=(String)spon1+";"+(String)spon2+";"+"0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;";
                    csvBody.get(row)[4] = sponsoredTop;
                }

            }



            row++;
            itr.next();
        }
        reader.close();

        // Write to CSV file which is open
        CSVWriter writer = new CSVWriter(new FileWriter(inputFile), CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END);
        writer.writeAll(csvBody);
        writer.flush();
        writer.close();
    }


    public void writeToWalletShoplifter2(String id,String filename, String promotions, Object spon1, Object spon2) throws IOException, CsvException {

        String classpathStr = System.getProperty("user.dir");
        String csvFile = classpathStr + "\\src\\test\\java\\sampletest\\dataFiles\\" + filename;
        File inputFile = new File(csvFile);
        CSVWriter writer1=null;
        // Read existing file
        if(!inputFile.exists()) {
            FileWriter fw = new FileWriter(inputFile, false);
            writer1=new CSVWriter(fw,CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END);
            writer1.writeNext(new String[]{"cid","ad","promotion","score","sponosoredTop"});

        }
        else {
            inputFile.createNewFile();
            FileWriter fw = new FileWriter(inputFile, true);
            writer1=new CSVWriter(fw,CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END);
        }

        String sponsoredTop="";
        String rank1="1.0;2.0;3.0;4.0;5.0;6.0;7.0;8.0;9.0;10.0;11.0;12.0;13.0;14.0;15.0;16.0;17.0;18.0;19.0;20.0";
            String[] rank=rank1.split(";");

            if(spon1 instanceof Integer && spon2 instanceof Integer) {
                int sponsored1 = (int) spon1;
                int sponsored2 = (int) spon2;
                for (int position = 0; position < rank.length; position++) {
                    if ((int) Float.parseFloat(rank[position]) == sponsored1 || (int) Float.parseFloat(rank[position]) == sponsored2) {
                        if (position == rank.length) {
                            sponsoredTop = sponsoredTop + "1";
                        } else {
                            sponsoredTop = sponsoredTop + "1;";
                        }
                    } else {
                        if (position == rank.length - 1) {
                            sponsoredTop = sponsoredTop + "0";
                        } else {
                            sponsoredTop = sponsoredTop + "0;";
                        }

                    }
                }
                writer1.writeNext(new  String[]{id,"test_Promo",promotions,rank1,sponsoredTop});
            }
            if( spon1 instanceof Integer && !(spon2 instanceof Integer)){
                {
                    int flagCounter=0;
                    int sponsored1 = (int) spon1;
                    String sponsored2 = (String) spon2;
                    for (int position = 0; position < rank.length; position++) {
                        if ((int) Float.parseFloat(rank[position]) == sponsored1 ) {
                            if (position == rank.length) {
                                sponsoredTop = sponsoredTop + "1";
                            } else {
                                sponsoredTop = sponsoredTop + "1;";
                            }
                        } else {
                            if (position == rank.length - 1) {
                                sponsoredTop = sponsoredTop + "0";
                            } else {
                                if(position !=sponsored1 && flagCounter==0 ){
                                    sponsoredTop = sponsoredTop +sponsored2+";";
                                    flagCounter++;
                                }else {
                                    sponsoredTop = sponsoredTop + "0;";
                                }
                            }

                        }
                    }
                    writer1.writeNext(new  String[]{id,"test_Promo",promotions,rank1,sponsoredTop});
                }

            }
            if( !(spon1 instanceof Integer) && (spon2 instanceof Integer)){
                {
                    int flagCounter=0;
                    String sponsored1 = (String) spon1;
                    int sponsored2 = (int) spon2;
                    for (int position = 0; position < rank.length; position++) {
                        if ((int) Float.parseFloat(rank[position]) == sponsored2) {
                            if (position == rank.length) {
                                sponsoredTop = sponsoredTop + "1";

                            } else {
                                sponsoredTop = sponsoredTop + "1;";
                                flagCounter++;
                            }
                        } else {


                            if (position == rank.length - 1) {
                                sponsoredTop = sponsoredTop + "0";
                            } else {
                                if(position !=sponsored2 && flagCounter==0 ){
                                    sponsoredTop = sponsoredTop +sponsored1+";";
                                    flagCounter++;
                                }else {
                                    sponsoredTop = sponsoredTop + "0;";
                                }
                            }

                        }
                    }
                    writer1.writeNext(new  String[]{id,"test_Promo",promotions,rank1,sponsoredTop});
                }

            }
            if( !(spon1 instanceof Integer) && !(spon2 instanceof Integer)){
                {
                    sponsoredTop=(String)spon1+";"+(String)spon2+";"+"0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;";
                    writer1.writeNext(new  String[]{id,"test_Promo",promotions,rank1,sponsoredTop});
                }

            }




        // Write to CSV file which is open
//        CSVWriter writer = new CSVWriter(new FileWriter(inputFile), CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END);
//        writer.writeAll(csvBody);
        writer1.flush();
        writer1.close();
    }
}
