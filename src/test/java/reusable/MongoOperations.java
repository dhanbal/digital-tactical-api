package reusable;

import com.mongodb.*;
import com.mongodb.MongoClient;
import com.mongodb.client.*;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.DeleteResult;
import org.bson.Document;
import org.json.JSONObject;

import java.util.Date;
import java.util.Properties;
import java.util.regex.Pattern;

public class MongoOperations {
    public static MongoClientURI uri;
    public static  MongoClient mongoClient;
    public static MongoIterable<String> dbnames;
    public static MongoDatabase db;

    private static String getConnectionURI() {
        ReadPropertyfiles rpf = new ReadPropertyfiles();
        Properties properties = rpf.prop();
        String user = properties.getProperty("user");
        String database = properties.getProperty("database");
        String strPassword = properties.getProperty("password");
        String host = properties.getProperty("host");
        String authMechanism = properties.getProperty("authMechanism");
        String replicaSet = properties.getProperty("replicaSet");

        String mongoUri = "mongodb://" + user + ":" + strPassword + "@" + host + "/" + database + "&authMechanism=" + authMechanism + "&replicaSet=" + replicaSet + "&readPreference=primary&connectTimeoutms=6000&socketTimeoutMS=300000?tls=true";

        return mongoUri;
    }
    // public static MongoIterable<String> getDatabaseConnection()
    static
    {
        try {
            uri=new MongoClientURI(getConnectionURI());
            mongoClient=new MongoClient(uri);
            dbnames=mongoClient.listDatabaseNames();
            db = mongoClient.getDatabase("omnimfd");
        }
        catch(MongoClientException mce){
            mce.printStackTrace();

        }
        catch (Exception e){
            e.printStackTrace();

        }
    }
    //Method To get promotions form shoplifterPromoion table
    public String getPromotions(String networkId) {
        FindIterable<Document> fi=null;
        MongoCursor<Document> cursor=null;
        String promotions = "";
        try {
            //fetch required collection
            MongoCollection<Document> collection = db.getCollection("shopliftrPromotion").withReadPreference(ReadPreference.primaryPreferred());
            // query
            BasicDBObject query = new BasicDBObject();
            query.put("retailerId", networkId);
            // mongo db find query
            fi = collection.find(query);
            cursor = fi.iterator();
            int count = 0;
                if(cursor!=null)
                while (cursor.hasNext() && count < 20) {
                    String json = cursor.next().toJson();
                    JSONObject document = new JSONObject(json);
                    promotions += document.get("_id").toString() + ";";
                    count++;
                }
                else{
                    System.out.println("No Promotion found");
                }

        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            cursor.close();
            System.out.println("get promotin result is"+promotions);
            return promotions.substring(0, promotions.length() - 1);
        }
    }

    public void removeShoplifterUserOffer(String networkId, String identifier, String flowType) {
        try {
            DeleteResult deleteResult=null;
            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("shopliftrUserOffer").withReadPreference(ReadPreference.primaryPreferred());
            // query parameters
            BasicDBObject query = new BasicDBObject();
            query.put("flowType", flowType);
            query.put("networkId", networkId);
            if (flowType == "KNOWN") {
                query.put("identifier", identifier);
            } else {
                query.put("identifier", identifier.toUpperCase());
            }
            System.out.println("Query for user delete "+ query);
             deleteResult = collection.deleteMany(query);
            System.out.println("No. of documents deleted : " + deleteResult.getDeletedCount());

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getCause());
        }
    }
    public boolean omniUserStatus(String networkId,String flowType, String id) {
        boolean status = false;
        FindIterable<Document> fi = null;
        MongoCursor<Document> cursor=null;
        Date updatedDate;
        Document document=null;
        long count = 0;
        try {
            MongoIterable<String> names = mongoClient.listDatabaseNames();
            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");
            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("omniUser").withReadPreference(ReadPreference.primaryPreferred());
            // query parameters
            BasicDBObject query = new BasicDBObject();
            System.out.println(networkId.split(Pattern.quote(".")).length+"   "+networkId+ networkId.split(".").length);
            if(networkId.contains(".")) {
                query.put("networkId", networkId.split(Pattern.quote("."))[0]);
                query.put("subnetwork",networkId.split(Pattern.quote("."))[1]);
            }else {
                query.put("networkId", networkId);
            }
            //
            //matches("[a-zA-Z]+")

                query.put("digitalId", id);
            if (flowType == "TV_KNOWN" || flowType == "TV_UNKNOWN") {
                query.put("flowType", flowType.replace("TV_", ""));
            } else {
                query.put("flowType", flowType);
            }
            System.out.println("QUERY USED: " + query);
            count = collection.countDocuments(query);

            if (count > 0) {
                String hashIs;
                fi = collection.find(query);
                if (fi!=null)
                    cursor = fi.iterator();
                //fetch hash in response with collection documents
                while (cursor.hasNext()) {
                    document=cursor.next();
                    hashIs = document.getString("hash");
                    updatedDate =document.getDate("updated");
                    System.out.println("Hash is "+hashIs+" Update date is "+updatedDate);

                    if(hashIs!=null){
                        status= true;
                    }
                }
//                status = true;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            status=false;
        }
        return status;

    }

    public boolean removeHashFromOmniUser(String networkId,String flowType, String id) {
        boolean status = false;
        FindIterable<Document> fi = null;
        MongoCursor<Document> cursor=null;
        Date updatedDate;
        Document document=null;
        long count = 0;
        try {
            MongoIterable<String> names = mongoClient.listDatabaseNames();
            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");
            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("omniUser").withReadPreference(ReadPreference.primaryPreferred());
            // query parameters
            BasicDBObject query = new BasicDBObject();
//            query.put("networkId", networkId);
            if(networkId.contains(".")) {
                query.put("networkId", networkId.split(Pattern.quote("."))[0]);
                query.put("subnetwork",networkId.split(Pattern.quote("."))[1]);
            }else {
                query.put("networkId", networkId);
            }
            //
            //matches("[a-zA-Z]+")
//            if (!id.contains("-")) {
//                query.put("fscId", id);
//            } else {
//                query.put("digitalId", id);
//            }
            query.put("digitalId", id);
            if (flowType == "TV_KNOWN" || flowType == "TV_UNKNOWN") {
                query.put("flowType", flowType.replace("TV_", ""));
            } else {
                query.put("flowType", flowType);
            }
            System.out.println("QUERY USED: " + query);
            count = collection.countDocuments(query);
            //collection.findOneAndUpdate();
            System.out.println("Document count is "+count);

            String hashIs = null;
            if (count > 0) {


                collection.updateOne(Filters.eq("digitalId", id), Updates.set("hash", ""));
                collection.updateOne(Filters.eq("digitalId", id), Updates.set("mfdTvPassworksCouponId", ""));
                fi = collection.find(query);
                if (fi!=null)
                    cursor = fi.iterator();
                //fetch hash in response with collection documents
                while (cursor.hasNext()) {
                    document=cursor.next();

                    hashIs = document.getString("hash");
                    //updatedDate =document.getDate("updated");
                    System.out.println("Hash is ---"+hashIs);

                }
                if(hashIs.isEmpty()) {
                    status = true;
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            status=false;
        }
        return status;

    }
    public void closeConnection( ){
        try {
            mongoClient.close();
            System.out.println("***  THANK YOU :) ***");
            if(mongoClient==null)
            System.out.println("Connection Closed--> Mongo Operations");
        }
        catch (Exception e){
            e.printStackTrace();
            System.out.println("Connection Closed--> Mongo Operations"+e.getMessage());
        }
    }

}
