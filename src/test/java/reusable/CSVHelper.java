package reusable;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvException;

import java.io.*;
import java.util.Iterator;
import java.util.List;

public class CSVHelper {

    public void createWalletFile(String identifier,String filename, String promotions, int sponsored1, int sponsored2) throws IOException, CsvException {

        String classpathStr = System.getProperty("user.dir");
        String csvFile = classpathStr + "\\src\\test\\java\\sampletest\\dataFiles\\" + filename;
        File inputFile=null;
        boolean fileStatus=false;
        try {
             inputFile = new File(csvFile);

             System.out.println(fileStatus=inputFile.exists()?true:false);
           if(!fileStatus){
               // creating new file when file doesn't exist
               inputFile.createNewFile();
               //inserting header and initial record

               CSVWriter csvWrite = new CSVWriter(new FileWriter(inputFile));
               String[] entries = {"4infohhid","ad","promotion","score"};
               String[] firstRow={"123","Ad","11111","2345"};
               csvWrite.writeNext(entries);
               csvWrite.writeNext(firstRow);
               csvWrite.close();
           }
            CSVReader reader = new CSVReader(new FileReader(inputFile));
            List<String[]> csvBody = reader.readAll();
            Iterator<String[]> itr = csvBody.iterator();
            int row = 0;
            while(itr.hasNext()){
                if(row == 0) {
                    row++;
                    itr.next();
                    continue;
                }
                // get CSV row column  and replace with by using row and column
                csvBody.get(row)[0] = identifier;
                csvBody.get(row)[2] = promotions;
                csvBody.get(row)[3] = "1.0;2.0;3.0;4.0;5.0;6.0;7.0;8.0;9.0;10.0;11.0;12.0;13.0;14.0;15.0;16.0;17.0;18.0;19.0;20.0";
                row++;
                itr.next();
            }
            reader.close();

            // Write to CSV file which is open
            CSVWriter writer = new CSVWriter(new FileWriter(inputFile), CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END);
            writer.writeAll(csvBody);
            writer.flush();
            writer.close();

        }
        catch (FileNotFoundException fnfe){
            fnfe.printStackTrace();
        }
        catch (IOException ioe){
            ioe.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();

        }

//        System.out.println(csvFile);


        // Read existing file
            }


    //
            public void createWalletFileWithMultipleLine(String[] identifiers,String filename, String promotions, int[] sponsored1, int[] sponsored2) throws IOException {
                    String classpathStr = System.getProperty("user.dir");
                    String csvFile = classpathStr + "\\src\\test\\java\\sampletest\\dataFiles\\" + filename;
                    File inputFile=null;
                    boolean fileStatus=false;
                    String score1="1.0;2.0;3.0;4.0;5.0;6.0;7.0;8.0;9.0;10.0;11.0;12.0;13.0;14.0;15.0;16.0;17.0;18.0;19.0;20.0";
                    FileWriter fw;
                   CSVWriter csvWrite=null;
                    try {
                        inputFile = new File(csvFile);
                        fw= new FileWriter(inputFile,false);
                        System.out.println(fileStatus=inputFile.exists()?true:false);
                        if(!fileStatus) {
                            // creating new file when file doesn't exist
                            inputFile.createNewFile();
                            System.out.println("File Created");
                            //inserting header and initial record
                        }
                             csvWrite = new CSVWriter(fw);
                            String[] entries = {"4infohhid","ad","promotion","score","sponsored"};
                            csvWrite.writeNext(entries);
//                            String[] firstRow={"123","Ad","11111","2345"};
                            for(int i=0;i<identifiers.length;i++) {
                                String[] data = {identifiers[i], "Ad-31032022", promotions, score1, "123Sponsored"};
                                csvWrite.writeNext(data);


                            }
                        csvWrite.close();
                        CSVReader reader = new CSVReader(new FileReader(inputFile));
                        String sponsoredTop="";
                        List<String[]> csvBody = reader.readAll();
                        Iterator<String[]> itr = csvBody.iterator();
                        int row = 0;
                        while(itr.hasNext()){
                            if(row == 0) {
                                row++;

                                itr.next();
                                continue;
                            }
//                            // get CSV row column  and replace with by using row and column
////                            csvBody.get(row)[0] = identifier;
////
////                              csvBody.get(row)[2] = promotions;
//                            //csvBody.get(row)[3] = "1.0;2.0;3.0;4.0;5.0;6.0;7.0;8.0;9.0;10.0;11.0;12.0;13.0;14.0;15.0;16.0;17.0;18.0;19.0;20.0";
////                            row++;
////                            itr.next();
////                        }
////                        reader.close();

                            System.out.println("Row is "+row);
                        String[] rank=csvBody.get(row)[3].split(";");

                        for(int position=0;position <rank.length; position++ ){
                            if((int)Float.parseFloat(rank[position])==sponsored1[1] || (int)Float.parseFloat(rank[position])== sponsored2[2]) {
                                if(position==rank.length){
                                    sponsoredTop =sponsoredTop +"1";
                                }else {
                                    sponsoredTop = sponsoredTop + "1;";
                                }
                            }
                            else {
                                if(position==rank.length-1){
                                    sponsoredTop=sponsoredTop+"0";
                                }else {
                                    sponsoredTop=sponsoredTop+"0;";
                                }

                            }
                        }
                        csvBody.get(row)[4] = sponsoredTop;

                        row++;
                        itr.next();
                    }
                        // Write to CSV file which is open
                        CSVWriter writer = new CSVWriter(new FileWriter(inputFile), CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END);
                        writer.writeAll(csvBody);
                        writer.flush();
                        writer.close();

                    }
                    catch (FileNotFoundException fnfe){
                        fnfe.printStackTrace();
                    }
                    catch (IOException ioe){
                        ioe.printStackTrace();
                    }
                    catch (Exception e){
                        e.printStackTrace();

                    }
                    finally {
                        if(csvWrite!=null)
                        csvWrite.close();
                    }
            }

}
