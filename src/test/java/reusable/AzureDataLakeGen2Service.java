package reusable;

import com.azure.storage.file.datalake.*;
import com.jcraft.jsch.*;
import com.mongodb.BasicDBObject;
import com.mongodb.ReadPreference;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.opencsv.CSVParser;
import com.opencsv.CSVReader;
import org.bson.Document;
import org.json.JSONObject;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;


public class AzureDataLakeGen2Service{
    Date today= Calendar.getInstance().getTime();
    ReadPropertyfiles rpf = new ReadPropertyfiles();
    Properties properties = rpf.prop();
    public  String Idomofilename=null;

    //Commented for testing purpose
    //public DataLakeServiceClient= new DataLakeServiceClientBuilder().endpoint("https://dls2deveastus2cdp.blob.core.windows.net/data-layer?sp=racwl&st=2021-08-20T12:35:43Z&se=2022-08-20T20:35:43Z&spr=https&sv=2020-08-04&sr=c&sig=JKm6i1fFEckNn3pSS%2FNqzfJlM6E3ExWJGMp8994sdaA%3D").buildClient();
    //    public static  void main(String args[]) throws IOException {
    //        AzureDataLakeGen2Service.getPromotionDetails("24");
    //    }
    public String getPromotionDetails(String networkId)  {
    AzureDataLakeGen2Service service=new AzureDataLakeGen2Service();
    Optional<byte[]> newFile= service.getBinaryFile(networkId);

      if(newFile.isPresent()) {
          try {
              FileOutputStream fos = new FileOutputStream("Promo_test.json");
        fos.write(newFile.get());
        System.out.println("New File created");
        return "Promo_test.json";
              }

           catch (FileNotFoundException fnfe) {
              fnfe.printStackTrace();
              return "File Not Created";
          }
          catch (IOException ioe) {
              ioe.printStackTrace();
              return "File Not Created";
          }
          catch (Exception e) {
              e.printStackTrace();
              return "File Not Created";
          }
      }
      else
      {
          System.out.println("No promotion file found");
          return "File Not Created";
      }

    }



    public DataLakeServiceClient storageGen2Client() {
        return new DataLakeServiceClientBuilder()


        .endpoint(properties.getProperty("adlGen2url"))
                .buildClient();
    }


    public Optional<byte[]> getBinaryFile(String networkId ) {
        try {
            DateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");

            String date=sdf.format(today);
            // Get a client that references a existing file-system
            DataLakeServiceClient client=storageGen2Client();
            DataLakeFileSystemClient fileSystemClient = client.getFileSystemClient( properties.getProperty("adlFileSystem"));
            //To be fetched from property file:- Dhananjaya
            //final String fullPath ="landing_zone/catalina/third_party_sources/shopliftr/promotions/shoplifter-ads-"+networkId+ "-"+date+".json";
            final String fullPath =properties.getProperty("promotionFilePath")+"shoplifter-ads-"+networkId+ "-"+date+".json";
            //log.info("Reading from ADL gen2 client file system: '{}'", fullPath);
            System.out.println("Reading from ADL gen2 client file system: '{}'"+ fullPath);
            // Initializes a new DataLakeFileClient object pointing to a given path in the file-system client
            DataLakeFileClient fileClient = fileSystemClient.getFileClient(fullPath);
            //Time Zone Validation
            if (!fileClient.exists()) {
              //  log.info("ADL gen2 client file system: Path '{}' doesn't exist !", fullPath);
                System.out.println("ADL gen2 client file system: Path '{}' doesn't exist !"+fullPath);
                return Optional.empty();
            }

            /* Download the file's content to output stream. */
            int dataSize = (int) fileClient.getProperties().getFileSize();
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream(dataSize);
            fileClient.read(outputStream);
            outputStream.close();
            //log.info("Reading from ADL gen2 client file system: '{}' finished successfully", fullPath);
            return Optional.of(outputStream.toByteArray());
        } catch (Exception ex) {
            throw new IllegalStateException(ex);
        }
    }


        public  boolean getFileFromSFTP(String country,String networkId,String flowType)
        {
            SimpleDateFormat sdf2=new SimpleDateFormat("yyyyMMdd");

                    //idomoo-USA24TVKN20220223_1.csv
            String targetIdomoofile="idomoo-"+country+networkId+"TVKN"+sdf2.format(today)+".csv";
            JSch jsch = new JSch();
            Session session = null;
            try {
                session = jsch.getSession("catalinausr", "sftp-usa.idomoo.com", 22);
                session.setConfig("StrictHostKeyChecking", "no");
                session.setPassword("NmM2ZjUwNzE1");
                session.connect();

                Channel channel = session.openChannel("sftp");
                channel.connect();
                ChannelSftp sftpChannel = (ChannelSftp) channel;

                sftpChannel.get("/retailer_1/downloads/idomoo-USA24TVKN20220225_1.csv",targetIdomoofile );
                sftpChannel.exit();
                session.disconnect();
                return true;
            } catch (JSchException e) {
                e.printStackTrace();
                return false;
            } catch (SftpException e) {
                e.printStackTrace();
                return false;
            }
            catch (Exception e){
                e.printStackTrace();
                return false;
            }
        }

    public String validateIdomooCSV(String networkId,String flowType,String cid){
        int count=getCSVCount(Idomofilename);
        MongoDatabase db= DBConnection.mongoClient.getDatabase("omnifd");
        String did=null;


        // Fetching the collection from the mongodb
        MongoCollection<Document> collection = db.getCollection("shopliftrUserOffer").withReadPreference(ReadPreference.primaryPreferred());
        BasicDBObject query = new BasicDBObject();
        if (flowType == "KNOWN" || flowType == "TV_KNOWN") {
            query.put("identifier", cid);
            query.put("flowType","TV_KNOWN");
        } else {
            query.put("identifier", did);
        }
        query.put("networkId",networkId);
        query.put("processed","ISODate(\"2022-02-24T00:00:00.000Z\")");
//        DB query:
//        db.getCollection('shopliftrUserOffer').find({"networkId" : "24",
//                "flowType" : "TV_KNOWN", "processed": ISODate("2021-10-03T00:00:00.000Z"), "identifier" : "USA-0024-100000130"}
        // query parameters

        System.out.println(query);

        // mongo db find query
        FindIterable<Document> fi = collection.find(query);
        MongoCursor<Document> cursor = fi.iterator();

        return null;
    }

    public int getCSVCount(String idomofilename) {

        int column = 0;
        String line;
        try {

            BufferedReader br = new BufferedReader(new FileReader(idomofilename));
            while ((line = br.readLine()) != null)   //returns a Boolean value
            {
                String[] employee = line.split(",");    // use comma as separator
                column = employee.length;
            }
            return column;
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
}
