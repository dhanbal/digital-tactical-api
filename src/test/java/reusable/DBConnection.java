package reusable;

import com.mongodb.*;
import com.mongodb.MongoClient;
import com.mongodb.client.*;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import jodd.json.JsonObject;
import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

import static org.apache.commons.lang.StringUtils.isNumeric;


public class DBConnection {
    public static MongoClientURI uri;
    public static  MongoClient mongoClient;
    public static MongoIterable<String> dbnames;

    private static String getConnectionURI() {
        ReadPropertyfiles rpf = new ReadPropertyfiles();
        Properties properties = rpf.prop();

        String user = properties.getProperty("user");
        String database = properties.getProperty("database");
        String strPassword = properties.getProperty("password");
        String host = properties.getProperty("host");
        String authMechanism = properties.getProperty("authMechanism");
        String replicaSet = properties.getProperty("replicaSet");

        String mongoUri = "mongodb://" + user + ":" + strPassword + "@" + host + "/" + database + "&authMechanism=" + authMechanism + "&replicaSet=" + replicaSet + "&readPreference=primary&connectTimeoutms=6000&socketTimeoutMS=300000?tls=true";

        return mongoUri;
    }
// public static MongoIterable<String> getDatabaseConnection()
     static
    {
        try {
             uri=new MongoClientURI(getConnectionURI());
             mongoClient=new MongoClient(uri);
             System.out.println("DB URI "+uri);
             dbnames=mongoClient.listDatabaseNames();
        }
        catch(MongoClientException mce){
            mce.printStackTrace();

        }
        catch (Exception e){
            e.printStackTrace();

        }
    }
    public String closeConnection(String close){
        try {
            mongoClient.close();
            System.out.println("***  THANK YOU :) ***");
            return  "DB CLOSED";
        }
        catch (Exception e){
            e.printStackTrace();
            return  "DB NOT CLOSED";
        }
    }
    public String getCollectionFromMongoDB(String data, String networkId) {
        String mongoUri = getConnectionURI();
        String result = "not matched";
        // string uri connection
        try {
            //Commented because DB connection created on Start of class

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("shopliftrPromotion").withReadPreference(ReadPreference.primaryPreferred());

            //Fetching and validating all the documents from the mongodb.
            if (getAllDocuments(collection, data, networkId)) {
                result = "matched";
            } else {
                result = "not matched";
            }
            // mongoClient.close();
        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }

    private static boolean getAllDocuments(MongoCollection<Document> col, String data, String networkId) {

        //Commented two line as we will have file path
//        JSONObject jsonObject = new JSONObject(data);
//        JSONArray jsonArray = jsonObject.getJSONArray("data");
        // System.out.println("JSON result data " + data);

        // query parameters
        BasicDBObject query = new BasicDBObject();
        query.put("retailerId", networkId);

        int count = 0;    // will count all the matched document
        boolean result = false;
        int lineCount=0;
        System.out.println("Fetching all documents from the collection");
        System.out.println("Query for promotion retrieval "+query);
        FindIterable<Document> fi = col.find(query);
        MongoCursor<Document> cursor = fi.iterator();

        // validate data in response with collection documents
        try {
            while (cursor.hasNext()) {
                String json = cursor.next().toJson();

//                System.out.println("Check for the values" + json);
                JSONObject dbObj = new JSONObject(json);
                //Adding new code for reading promotion file line by line

                BufferedReader br = new BufferedReader(new FileReader(data)) ;
                String line;
                 lineCount=0;
                while ((line = br.readLine()) != null) {
                    JSONObject fileObj=new JSONObject(new String(line));
                    lineCount++;
                boolean match=false;

                /*for (int i = 0; i < jsonArray.length(); i++) {
                    boolean match = false;
                    JSONObject fileObj = (JSONObject) jsonArray.get(i);*/

                    if (dbObj.get("upc").equals(fileObj.get("upc"))
                            && dbObj.get("_id").equals(fileObj.get("shopliftr_id"))
                            && dbObj.get("brand").equals(fileObj.get("brand"))
                            && dbObj.get("name").equals(fileObj.get("name"))
                            && dbObj.get("type").equals(fileObj.get("type"))
                            && dbObj.get("additional").equals(fileObj.get("additional"))
                            && dbObj.get("manufacturer").equals(fileObj.get("manufacturer"))
                            //as size has been removed from promotion
//                            && dbObj.get("size").equals(fileObj.get("size"))
                            && dbObj.get("customPrice").equals(fileObj.get("custom_price"))
                            && dbObj.get("countryCode").equals(fileObj.get("country_code"))
                            && dbObj.get("imageUrl").equals(fileObj.get("image_url"))
                            && dbObj.get("saleQty").equals(fileObj.get("sale_qty"))) {

                        count++;
                    }
                }
            }


        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }finally {
            cursor.close();

            System.out.println(count + " " + lineCount);

            if (count == lineCount) {
                result = true;
            } else {
                result = false;
            }

            return result;
        }
    }

    public String getCurrentDateTime(int minutesToSubtract) throws ParseException {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime datetime = LocalDateTime.now(ZoneOffset.UTC);
//            System.out.println(datetime);
        datetime = datetime.minusMinutes(minutesToSubtract);
        String aftersubtraction = datetime.format(formatter);
//            System.out.println(aftersubtraction);
        String currentDate = aftersubtraction.substring(0, 10);
        String currentTime = aftersubtraction.substring(11, 19);
        String dateStr = currentDate + " " + currentTime + ".00 UTC";
//        System.out.println(dateStr);
        return dateStr;
    }


    public String validateJobStatus(String jobName, String networkId) {
        //String mongoUri = getConnectionURI();
        String result = "";
        String jobStatus = "";
        String jobExitStatusCode = "";
        try {
            MongoIterable<String> names = mongoClient.listDatabaseNames();
            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("shopliftr-batch").withReadPreference(ReadPreference.primaryPreferred());

            // Get current date time
            String dateStr = getCurrentDateTime(5);
            Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS ZZZ").parse(dateStr);
            System.out.println(date);

            // query parameters
            BasicDBObject query = new BasicDBObject();
            query.put("jobInstance.jobName", jobName);
            query.put("jobParameters.countryCode.parameter", "USA");
            query.put("jobParameters.networkId.parameter", networkId);
            query.put("startTime", new BasicDBObject("$gte", date));
            System.out.println(query);

            // mongo db find query
            FindIterable<Document> fi = collection.find(query);
            MongoCursor<Document> cursor = fi.iterator();

            try {
                while (cursor.hasNext()) {
                    String data = cursor.next().toJson();
                    System.out.println("Data " + data);
                    JSONObject json = new JSONObject(data);
                    jobStatus = json.getString("status");
                    JSONObject jobExitStatus = json.getJSONObject("exitStatus");
//                    JSONObject obj = new JSONObject(jobExitStatus);
                    jobExitStatusCode = jobExitStatus.getString("exitCode");
                    System.out.println("Status of Job Run is " + jobStatus);
                    System.out.println("Exit status of Job Run is " + jobExitStatusCode);
                }

                if (jobStatus.equals("COMPLETED") && jobExitStatusCode.equals("COMPLETED")) {
                    result = "COMPLETED";
                } else {
                    result = "NOT COMPLETED";
                }

            } finally {
                cursor.close();
            }
            //mongoClient.close();
        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }


    public String validateJobProperties(String jobName, String networkId, String directory, String flowType) {
        String mongoUri = getConnectionURI();
        String result = "not matched";
        // string uri connection
        try {


            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("shopliftrJobProperties").withReadPreference(ReadPreference.primaryPreferred());

            // get current date time
            String dateStr = getCurrentDateTime(5);
            Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS ZZZ").parse(dateStr);
            System.out.println(date);

            // query parameters
            BasicDBObject query = new BasicDBObject();
            query.put("jobId", jobName);
            query.put("countryCode", "USA");
            query.put("networkId", networkId);
            query.put("flowType", flowType);
//            query.put("updated", new BasicDBObject("$gte", date));
//            System.out.println(query);

            // mongo db find query
            FindIterable<Document> fi = collection.find(query);
            MongoCursor<Document> cursor = fi.iterator();

            while (cursor.hasNext()) {
                String data = cursor.next().toJson();
//                System.out.println("Data : " + data);
                JSONObject jsonObj = new JSONObject(data);
                String currentDate = dateStr.substring(0, 10);
                String directoryName = directory + networkId + "_" + currentDate;
                System.out.println("Expected Directory "+directoryName);
                System.out.println("Actual Directory "+jsonObj.getString("directoryName"));
                if (jsonObj.get("directoryName").equals(directoryName)) {
                    result = "matched";
                } else {
                    result = "not matched";
                }
            }

            //mongoClient.close();
        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }

    public String validateZipcodes(String zipCodes, String networkId) {
        String mongoUri = getConnectionURI();
        String result = "not matched";
        // string uri connection
        try {

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("store").withReadPreference(ReadPreference.primaryPreferred());

            JSONArray zipcodes = new JSONArray(zipCodes);

            int count = 0;  // will count all the matched document

            for (int i = 0; i < zipcodes.length(); i++) {

                JSONObject obj = zipcodes.getJSONObject(i);

                // query parameters
                BasicDBObject query = new BasicDBObject();
                query.put("retailerId", networkId);
                query.put("storeId", obj.get("siteId"));
                query.put("postalCode", obj.get("zipcode"));
                query.put("city", obj.get("city"));

                // mongo db count query
                long resultCount = collection.countDocuments(query);

                if (resultCount > 0) {
                    count++;
                }
            }

            System.out.println("Count : " + count);
            if (count == zipcodes.length()) {
                result = "matched";
            } else {
                result = "not matched";
            }
            //mongoClient.close();
        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return result;
        }

    }

    public String validateZipcodeOffers(String zipcodeOffersJson) {
        String mongoUri = getConnectionURI();
        String result = "";
        try {
            //Commented because DB connection created on Start of class
//            MongoClientURI uri = new MongoClientURI(mongoUri);
//            MongoClient mongoClient = new MongoClient(uri);
//            System.out.println("Mongo client connection successful");

            MongoIterable<String> names = mongoClient.listDatabaseNames();
            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("shopliftrPromotion").withReadPreference(ReadPreference.primaryPreferred());
            JSONArray zipcodeOffers = new JSONArray(zipcodeOffersJson);
//            System.out.println("zipcode offers " + zipcodeOffers);

            // get current date time
            String dateStr = getCurrentDateTime(2);
            Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS ZZZ").parse(dateStr);
            System.out.println(date);

            int count = 0;  // will count all the matched document

            for (int i = 0; i < zipcodeOffers.length(); i++) {

                JSONObject obj = (JSONObject) zipcodeOffers.get(i);
                String zipcode = obj.getString("zipcode");
                long offersCount = obj.getLong("count");
                System.out.println("Offer count : " + offersCount + "for zipcode : " + zipcode);
                JSONArray offers = obj.getJSONArray("offers");

                // query parameters
                BasicDBObject query = new BasicDBObject();
                query.put("zipCodes", zipcode);
                query.put("saleStart", new BasicDBObject("$gte", date));
//              System.out.println(query);

                // mongo db count query
                long resultCount = collection.countDocuments(query);
                System.out.println(resultCount);

                // mongo db find query
                FindIterable<Document> fi = collection.find(query);
                MongoCursor<Document> cursor = fi.iterator();

                int offersMatch = 0;  // matched offers count

                try {
                    while (cursor.hasNext()) {
                        String json = cursor.next().toJson();
//                        System.out.println("Mongo document : " + json);

                        JSONObject dbObj = new JSONObject(json);

                        for (int j = 0; j < offers.length(); j++) {

                            JSONObject fileObj = (JSONObject) offers.get(j);
//                            System.out.println(fileObj);

                            if (dbObj.get("upc").equals(fileObj.get("upc"))
                                    && dbObj.get("_id").equals(fileObj.get("id"))
                                    && dbObj.get("brand").equals(fileObj.get("brand"))
                                    && dbObj.get("name").equals(fileObj.get("name"))
                                    && dbObj.get("type").equals(fileObj.get("type"))
                                    && dbObj.get("additional").equals(fileObj.get("additional"))
                                    && dbObj.get("manufacturer").equals(fileObj.get("manufacturer"))
                                    // && dbObj.get("size").equals(fileObj.get("size"))
                                    && dbObj.get("customPrice").equals(fileObj.get("custom_price"))
                                    && dbObj.get("imageUrl").equals(fileObj.get("image_url"))
                                //   && dbObj.get("saleQty").equals(fileObj.get("sale_qty"))
                            ) {

                                offersMatch++;
                            }
                        }
                    }

                } finally {
                    cursor.close();
                    System.out.println("Offers Match: " + offersMatch + " offers length : " + offers.length());
                    if (resultCount == offersCount && offersMatch == offers.length()) {
                        count++;
                    }
                }
            }
            System.out.println("Matched Count: " + count + " zipcodeOffers length : " + zipcodeOffers.length());
            if (count == zipcodeOffers.length()) {
                result = "matched";
            } else {
                result = "not matched";
            }

            //mongoClient.close();
        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }

    public String validateWalletFileContent(String walletFileContent, String networkId, String flowType) {
      //  String mongoUri = getConnectionURI();

        String result = "";
        try {

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("shopliftrUserOffer").withReadPreference(ReadPreference.primaryPreferred());

            // parse wallet file content into json object
            JSONObject obj = new JSONObject(walletFileContent);
            JSONArray walletJson = obj.getJSONArray("data");
            int matchedCounts = 0;  // matched wallet file row count

            // get current date and time
            String dateStr = getCurrentDateTime(5);
            Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS ZZZ").parse(dateStr);
            System.out.println("Date value is "+date);

            for (int i = 0; i < walletJson.length(); i++) {

                // parse wallet file data into json object
                JSONObject json = walletJson.getJSONObject(i);
                String identifier = "";
                if (flowType == "KNOWN" || flowType=="TV_KNOWN") {
                    identifier = json.getString("cid");
                } else {
                    identifier = json.getString("did");
                }

                JSONArray walletOffers = json.getJSONArray("promotion");
                JSONArray walletScore = json.getJSONArray("score");
                JSONArray walletSponsored = json.getJSONArray("sponsored");
                // query parameters
                BasicDBObject query = new BasicDBObject();
                if (flowType == "KNOWN" || flowType=="TV_KNOWN") {
                    query.put("identifier", identifier);
                } else {
                    query.put("identifier", identifier.toUpperCase());
                }
                query.put("networkId", networkId);
                query.put("flowType", flowType);
//                query.put("created", new BasicDBObject("$gte", date));

                // mongo db find query
                FindIterable<Document> fi = collection.find(query);
                MongoCursor<Document> cursor = fi.iterator();

                String data = "";

                try {

                    while (cursor.hasNext()) {
                        data = cursor.next().toJson();
                    }
                    JSONObject dataObj = new JSONObject(data);
                    JSONArray offers = dataObj.getJSONArray("offers");
                    System.out.println("Offer from DB is"+ offers.toString());
                    AtomicInteger countMatchedOffers = new AtomicInteger();
                   // matched offers count
                    Map<Integer,String> offerRankMap=getOfferMap(walletOffers,walletScore);
                    //Code Added by Dhananjaya
                    boolean status;
                    offerRankMap.forEach((rank,promotionValue)->{
                          if(offers.getJSONObject(rank-1).getString("promotion").equals(promotionValue)) {
                            System.out.println("Rank matching status is passed");
                              countMatchedOffers.getAndIncrement();

                        }
                    });

                    System.out.println("Count Offers Match : " + countMatchedOffers);
                    if (countMatchedOffers.get() == offers.length()) {
                        matchedCounts++;
                    }
                } finally {
                    System.out.println("Match CID counts : " + matchedCounts);
                    cursor.close();
                }

            }
            if (matchedCounts == walletJson.length()) {
                result = "matched";
            } else {
                result = "not matched";
            }
    //            mongoClient.close();
        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }
    public Map<Integer, String> getOfferMap(JSONArray walletOffers, JSONArray walletScore) {

        String[] walletScore1=walletScore.toString().replace("[","").replace("]","").replace("\"","").split(",");

        String[] walletOffers1=walletOffers.toString().replace("[","").replace("]","").replace("\"","").split(",");

        Map<Integer,String> offerRankMap1=new HashMap<>();
        // int NoOfOffers=walletOffers.;
        for(int i=0;i<20;i++)
        {
            offerRankMap1.put((int)Double.parseDouble(walletScore1[i]),walletOffers1[i]);
        }


        return offerRankMap1;
    }
    public String validateWalletFileContentSponsored(String walletFileContent, String networkId, String flowType) {
        String result = "";
        try {
            MongoIterable<String> names = mongoClient.listDatabaseNames();
            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");
            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("shopliftrUserOffer").withReadPreference(ReadPreference.primaryPreferred());
            // parse wallet file content into json object
            JSONObject obj = new JSONObject(walletFileContent);
            JSONArray walletJson = obj.getJSONArray("data");
            int matchedCounts = 0;  // matched wallet file row count

            // get current date and time
            String dateStr = getCurrentDateTime(5);
            Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS ZZZ").parse(dateStr);
            System.out.println("Date value is "+date);
            Map<Integer,String> offerRankMap = new HashMap<>();
            for (int i = 0; i < walletJson.length(); i++) {

                // parse wallet file data into json object
                JSONObject json = walletJson.getJSONObject(i);
                String identifier = "";
                //Create a DB query object
                BasicDBObject query = new BasicDBObject();
                if (flowType == "KNOWN" || flowType=="TV_KNOWN") {
                    identifier = json.getString("cid");
                    query.put("identifier", identifier);
                } else {
                    identifier = json.getString("did");
                    query.put("identifier", identifier.toUpperCase());
                }

                JSONArray walletOffers = json.getJSONArray("promotion");
                JSONArray walletScore = json.getJSONArray("score");
                JSONArray walletSponsored = json.getJSONArray("sponsored");
//
//                if (flowType == "KNOWN" || flowType=="TV_KNOWN") {
//                    query.put("identifier", identifier);
//                } else {
//                    query.put("identifier", identifier.toUpperCase());
//                }
                //Set query params
                query.put("networkId", networkId);
                query.put("flowType", flowType);
                // mongo db find query
                FindIterable<Document> fi = collection.find(query);
                MongoCursor<Document> cursor = fi.iterator();
                String data = "";

                try {
                    while (cursor.hasNext()) {
                        data = cursor.next().toJson();
                    }
                    JSONObject dataObj = new JSONObject(data);
                    JSONArray offers = dataObj.getJSONArray("offers");
                    System.out.println("Offer from DB is"+ offers.toString());
                    AtomicInteger countMatchedOffers = new AtomicInteger();
                    // matched offers count
                    //Map<Integer,String>
                            offerRankMap=getOfferMapSponsored(walletScore,walletSponsored);
                    //Code Added by Dhananjaya
                    boolean status;
                    offerRankMap.forEach((rank,sponsoredFlag)->{
                        if(Integer.parseInt(sponsoredFlag)==1){
                            if(offers.getJSONObject(rank-1).getInt("flg")==1){
                                System.out.println("Rank matching status is passed");
                                countMatchedOffers.getAndIncrement();

                            }
                        }
                    });


                    System.out.println("TOTAL 1 IS"+offerRankMap.values().stream().filter(v -> Integer.parseInt(v) ==1).count());
                    System.out.println("TOTAL 0 IS"+offerRankMap.values().stream().filter(v -> Integer.parseInt(v) ==0).count());

                    System.out.println("Count Offers Match : " + countMatchedOffers);
                    matchedCounts=countMatchedOffers.get();
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                finally {
                    System.out.println("Match CID counts : " + matchedCounts);
                    cursor.close();
                }

            }
            //max sponsored match 2
            // if (matchedCounts == walletJson.length()) previously matching was done against all promotion i,e 20 , now max sponsored ad is 2



                if (matchedCounts ==   offerRankMap.values().stream().filter(v -> Integer.parseInt(v) ==1).count()){
                result = "matched";
            } else {
                result = "not matched";
            }
            //            mongoClient.close();
        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }

    public String validateWalletFileContentHpo(String walletFileContent, String networkId, String flowType) {
        String result = "";
        try {
            MongoIterable<String> names = mongoClient.listDatabaseNames();
            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");
            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("hpoUserOffer").withReadPreference(ReadPreference.primaryPreferred());
            // parse wallet file content into json object
            JSONObject obj = new JSONObject(walletFileContent);
            JSONArray walletJson = obj.getJSONArray("data");
            int matchedCounts = 0;  // matched wallet file row count

            // get current date and time
            String dateStr = getCurrentDateTime(5);
            Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS ZZZ").parse(dateStr);
            System.out.println("Date value is "+date);
            Map<Integer,String> offerRankMap = new HashMap<>();
            for (int i = 0; i < walletJson.length(); i++) {

                // parse wallet file data into json object
                JSONObject json = walletJson.getJSONObject(i);
                String identifier = "";
                //Create a DB query object
                BasicDBObject query = new BasicDBObject();
                if (flowType == "KNOWN" || flowType=="TV_KNOWN") {
                    identifier = json.getString("cchid");
                    query.put("identifier", identifier.replace("-1500","-0024"));
                } else {
                    identifier = json.getString("did");
                    query.put("identifier", identifier.toUpperCase());
                }

                JSONArray walletOffers = json.getJSONArray("external_id");
                JSONArray walletScore = json.getJSONArray("score");

                //Set query params
                query.put("networkId", networkId);
                query.put("flowType", flowType);
                // mongo db find query
                FindIterable<Document> fi = collection.find(query);
                MongoCursor<Document> cursor = fi.iterator();
                String data = "";

                try {
                    while (cursor.hasNext()) {
                        result="matched";
                        data = cursor.next().toJson();

                        JSONObject dataObj = new JSONObject(data);
                        JSONArray offers = dataObj.getJSONArray("offers");
                        System.out.println("Offer from DB is" + offers.toString());

                    }
                    // matched offers count
                    //Map<Integer,String>
                }
                catch (Exception e){
                    e.printStackTrace();
                    result="not matched";
                }
                finally {
                    cursor.close();
                }

            }

        } catch (Exception e) {
            result="not matched";
            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }
    public Map<Integer, String> getOfferMapSponsored(JSONArray walletScore,JSONArray walletSponsored) {

        String[] walletScore1=walletScore.toString().replace("[","").replace("]","").replace("\"","").split(",");

        String[] walletSponsored1=walletSponsored.toString().replace("[","").replace("]","").replace("\"","").split(",");

        Map<Integer,String> offerRankMap1=new HashMap<>();
        // int NoOfOffers=walletOffers.;
        for(int i=0;i<20;i++)
        {
            if((int)Double.parseDouble(walletScore1[i])>5 ||  !isNumeric(walletSponsored1[i]) )
            {
                walletSponsored1[i]= String.valueOf(0);
            }
            //put score and corresponding sponsored flag in map
            offerRankMap1.put((int)Double.parseDouble(walletScore1[i]),walletSponsored1[i]);

        }
        return offerRankMap1;
    }


    public String validateCeltraOffers(String celtraResponse, String networkId, String identifier, String flowType, int sponsored1, int sponsored2) {
        String mongoUri = getConnectionURI();
        String result = "";
        try {
            MongoIterable<String> names = mongoClient.listDatabaseNames();
            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("shopliftrUserOffer").withReadPreference(ReadPreference.primaryPreferred());
            MongoCollection<Document> collection1 = db.getCollection("shopliftrOffer").withReadPreference(ReadPreference.primaryPreferred());
            // query parameters
            BasicDBObject query = new BasicDBObject();
            query.put("flowType", flowType);
            query.put("networkId", networkId);
            if (flowType == "KNOWN") {
                query.put("identifier", identifier);
            } else {
                query.put("identifier", identifier.toUpperCase());
            }
            System.out.println("Shoplifter user offer "+query);
            // parse celtra response in json
            JSONObject celtraJson = new JSONObject(celtraResponse);
            JSONArray offers = new JSONArray();

            // mongo db find query
            FindIterable<Document> fi = collection.find(query);
            MongoCursor<Document> cursor = fi.iterator();

            JSONObject jsonData=null;
            try {
                while (cursor.hasNext()) {
                    String data = cursor.next().toJson();
                    jsonData = new JSONObject(data);
                    offers = jsonData.getJSONArray("offers");
//                    System.out.println("Offers Array is : " + offers);
                }

                int count = 0;  // matched offers count
                int totalceltraOffers = 1;
                // count total offers present in celtra response
                while (true) {
                    String offerId = "offer_id_" + totalceltraOffers;
                    try {
                        celtraJson.get(offerId);
                        totalceltraOffers++;
                    } catch (JSONException e) {
                        totalceltraOffers--;
                        break;
                    }
                }

                System.out.println("Total Celtra Offers : " + totalceltraOffers);

//                System.out.println(query2);
                for (int i = 1; i <= totalceltraOffers; i++) {
                    String name = "name_" + i;
                    String promotion = "offer_id_" + i;
                    String brand = "brand_" + i;
                    String shopliftrAdId = "shopliftr_ad_id_" + i;
                    String manufacturer = "manufacturer_" + i;
                    String customPrice = "custom_price_" + i;
                    String size = "size_" + i;
                    String saleQty = "sale_qty_" + i;
                    String type = "type_" + i;
                    String imageUrl = "image_url_" + i;
                    String sponsored=null;
                    if(sponsored1 <=5 && sponsored2 > 5) {
                        if (i == sponsored1) {
                            sponsored = "sponsored_" + i;
                        }
                    }
                    if(sponsored1 >5 && sponsored2 <=5) {
                        if ( i == sponsored2) {
                            sponsored = "sponsored_" + i;
                        }
                    }
                    if(sponsored1 <=5 && sponsored2 <=5) {
                        if (i == sponsored1 || i == sponsored2) {
                            sponsored = "sponsored_" + i;
                        }
                    }
                    if(sponsored1 >5 && sponsored2>5) {
                       sponsored=null;
                    }


                    for (int j = 0; j < offers.length(); j++) {
                        //validating sponsored ad
                        if(sponsored!=null){
                            System.out.println(celtraJson.getString(sponsored));
                             if( !celtraJson.getString(sponsored).equalsIgnoreCase("sponsored"))
                            {
                                return "NOT MATCHED";
                            }
                        }
                        JSONObject offersJson = (JSONObject) offers.get(j);
                        BasicDBObject query2=new BasicDBObject();

                        TimeZone tz = TimeZone.getTimeZone("UTC");
                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm.sss'Z'");
                        df.setTimeZone(tz);
                        String processDate = df.format(new Date(Long.parseLong(jsonData.getJSONObject("processed").get("$date").toString())));

                        System.out.println("processed date is "+processDate);

                        //System.out.println("Processed Date is "+new Date(Long.parseLong(jsonData.getString("processed"))));



                        SimpleDateFormat formater=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS ZZZ");
                        formater.setTimeZone(TimeZone.getTimeZone("UTC"));
                        Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS ZZZ").parse(formater.format(new Date((Long) jsonData.getJSONObject("processed").get("$date"))));

                         query2.put("processed",date);
                        query2.put("networkId",jsonData.getString("networkId"));
                        query2.put("offerId",offersJson.getString("offerId"));
                        System.out.println("Query 2 is" +query2);
                        FindIterable<Document> offerIterable=collection1.find(query2);

                        //Document query6= (Document) Filters.eq("processed",processDate);

                        MongoCursor<Document> cursor2=offerIterable.iterator();
                        JSONObject jsonDataOffer=null;
                        if(cursor2.hasNext())
                        {
                             jsonDataOffer=new JSONObject(cursor2.next().toJson());
                             System.out.println("Data offer is" +jsonDataOffer);
                        }
                        System.out.println("--"+jsonDataOffer.getString("promotion")+"-----"+celtraJson.getString(promotion));
                          if( jsonDataOffer.getString("promotion").equalsIgnoreCase(celtraJson.getString(promotion)))
                        {
                            // new changes
                            // query parameters
                            //commented by Dhananjaya

                            if (jsonDataOffer.get("brand").equals(celtraJson.get(brand))
                                    && (jsonDataOffer.get("shopliftrAdId").equals(celtraJson.get(shopliftrAdId)))
                                    && (jsonDataOffer.get("manufacturer").equals(celtraJson.get(manufacturer)))
                                    && (jsonDataOffer.get("customPrice").equals(celtraJson.get(customPrice)))
                                    && (jsonDataOffer.get("size").equals(celtraJson.get(size)))
                                    && (jsonDataOffer.get("saleQty").equals(celtraJson.get(saleQty)))
                                    && (jsonDataOffer.get("type").equals(celtraJson.get(type)))
                            ) {
                                count++;
                                break;
                            }


                        }
                    }
                }

                System.out.println("Total Matched count : " + count);

                if (count == totalceltraOffers) {
                    result = "MATCHED";
                } else {
                    result = "NOT MATCHED";
                }

            }
            catch (Exception e){
                e.printStackTrace();
            }finally {
                cursor.close();
            }
         //   mongoClient.close();
        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }

    public void clearShopliftrUserOffer(String networkId, String identifier, String flowType) {
        String mongoUri = getConnectionURI();
        try {
            MongoClientURI uri = new MongoClientURI(mongoUri);
            MongoClient mongoClient = new MongoClient(uri);
            System.out.println("Mongo client connection successful");

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("shopliftrUserOffer").withReadPreference(ReadPreference.primaryPreferred());

            // query parameters
            BasicDBObject query = new BasicDBObject();
            query.put("flowType", flowType);
            query.put("networkId", networkId);
            if (flowType == "KNOWN") {
                query.put("identifier", identifier);
            } else {
                query.put("identifier", identifier.toUpperCase());
            }
            System.out.println(query);

            DeleteResult deleteResult = collection.deleteMany(query);

            System.out.println("No. of documents deleted : " + deleteResult.getDeletedCount());

         //   mongoClient.close();

        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
        }
    }


    public void clearhpoUserOffer(String networkId, String identifier, String flowType) {
        String mongoUri = getConnectionURI();
        try {
            MongoClientURI uri = new MongoClientURI(mongoUri);
            MongoClient mongoClient = new MongoClient(uri);
            System.out.println("Mongo client connection successful");

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("hpoUserOffer").withReadPreference(ReadPreference.primaryPreferred());

            // query parameters
            BasicDBObject query = new BasicDBObject();
            query.put("flowType", flowType);
            query.put("networkId", networkId);
            if (flowType == "KNOWN") {
                query.put("identifier", identifier);
            } else {
                query.put("identifier", identifier.toUpperCase());
            }
            System.out.println(query);

            DeleteResult deleteResult = collection.deleteMany(query);

            System.out.println("No. of documents deleted : " + deleteResult.getDeletedCount());

            //   mongoClient.close();

        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
        }
    }

    public String validateOmniUser(String did, String networkId, String vaultLocation, String flowType) {
        String mongoUri = getConnectionURI();
        String result = "not matched";
        String subnetwork = "";
        try {
            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("omniUser").withReadPreference(ReadPreference.primaryPreferred());

            // new subnetwork changes
            if (networkId.contains(".")) {
                String networkdetails[] = networkId.split("\\.");
                networkId = networkdetails[0];
                subnetwork = networkdetails[1];
            }

            // query parameters
            BasicDBObject query = new BasicDBObject();

            if (subnetwork == "") {
                query.put("networkId", networkId);
                query.put("digitalId", did.toUpperCase());
            } else {
                query.put("networkId", networkId);
                query.put("subnetwork", subnetwork);
                query.put("digitalId", did.toUpperCase());
            }
            System.out.println("Query :" + query);

            // mongo db count query
            long documentCount = collection.countDocuments(query);

            System.out.println("Document Count " + documentCount);

            boolean match = false;
            int locationCount = 0;
            int vaultLocationLength = 0;


            if (flowType == "UNKNOWN") {
                match = true;
            } else {
                // parse vault response into json array
                JSONArray vaultLocationArray = new JSONArray(vaultLocation);
                vaultLocationLength = vaultLocationArray.length();

                // mongo db find query
                FindIterable<Document> fi = collection.find(query);
                MongoCursor<Document> cursor = fi.iterator();
                String data = "";
                while (cursor.hasNext()) {
                    data = cursor.next().toJson();
//                        System.out.println("Data : " + data);
                }
                if(data!=null){
                    match=true;
                }

                try {
                    while (cursor.hasNext()) {
                        data = cursor.next().toJson();
//                        System.out.println("Data : " + data);
                    }
                    JSONObject json = new JSONObject(data);
                    if(json!=null){
                        match =true;
                    }
//                    JSONObject locationCache = json.getJSONObject("locationCache");
//                    JSONArray locations = locationCache.getJSONArray("locations");

//                    for (int j = 0; j < vaultLocationArray.length(); j++) {
//
//                        JSONObject vaultObj = vaultLocationArray.getJSONObject(j);
////                            System.out.println(vaultObj);
//                        for (int i = 0; i < locations.length(); i++) {
//                            JSONObject obj = (JSONObject) locations.get(i);
//
//                            if (obj.getDouble("latitude") == vaultObj.getDouble("latitude")
//                                    && obj.getDouble("longitude") == vaultObj.getDouble("longitude")) {
//                                locationCount++;
//                                match = true;
//                                break;
//                            }
//                        }
//                    }
                }
                finally {
                    cursor.close();
                }
            }
            if (documentCount > 0  && match == true) {
                result = "matched";
            } else {
                result = "not matched";
            }

            //mongoClient.close();
        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }

    public String validateUnknownUserOffer(String walletJson, String networkId) {
        String mongoUri = getConnectionURI();
        String result = "not matched";

        try {

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("unknownUserOffer").withReadPreference(ReadPreference.primaryPreferred());

            // parse wallet data into json
            JSONObject json = new JSONObject(walletJson);
            JSONArray data = json.getJSONArray("data");

            int matchedCounts = 0;

            for (int i = 0; i < data.length(); i++) {

                JSONObject obj = data.getJSONObject(i);
                JSONArray walletPromotionArray = obj.getJSONArray("promotion");
                JSONArray walletScore = obj.getJSONArray("score");
//                System.out.println("Wallet promotion Array : " + walletPromotionArray);

                String did = obj.getString("did").toUpperCase();

                // query parameters
                BasicDBObject dbobj = new BasicDBObject();
                dbobj.put("networkId", networkId);
                dbobj.put("did", did);
//                System.out.println("Query :"+dbobj);

                // mongo db find query
                FindIterable<Document> fi = collection.find(dbobj);
                MongoCursor<Document> cursor = fi.iterator();
                String document = "";

                try {
                    while (cursor.hasNext()) {
                        document = cursor.next().toJson();
                    }
                    JSONObject jsonData = new JSONObject(document);
                    JSONArray offers = jsonData.getJSONArray("offers");
//                    System.out.println("Offers " + offers);

                    int matchedOffers = 0;

                    for (int j = 0; j < walletPromotionArray.length(); j++) {
                        String promo = walletPromotionArray.getString(j);
                        String score = walletScore.getString(j);
//                        System.out.println("Promo " + promo);

                        for (int k = 0; k < offers.length(); k++) {
                            JSONObject offer = offers.getJSONObject(k);

                            if (offer.get("promotion").equals(promo)
                                    && offer.getInt("rank") == Integer.parseInt(score.substring(0, score.length() - 2))) {
                                matchedOffers++;
                                break;
                            }
                        }
                    }
                    System.out.println("total Matched offers " + matchedOffers);
                    if (matchedOffers == walletPromotionArray.length()) {
                        matchedCounts++;
                    }
                } finally {
                    cursor.close();
                }
                System.out.println("total Matched Data " + matchedCounts);
                if (matchedCounts == data.length()) {
                    result = "matched";
                } else {
                    result = "not matched";
                }
            }

            //mongoClient.close();
        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }

    public String validateVaultPromotion(String walletJson) {
        String mongoUri = getConnectionURI();
        String result = "not matched";

        try {
//            MongoClientURI uri = new MongoClientURI(mongoUri);
//            MongoClient mongoClient = new MongoClient(uri);
//            System.out.println("Mongo client connection successful");

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("vaultPromotion").withReadPreference(ReadPreference.primaryPreferred());

            // parse wallet data into json object
            JSONObject json = new JSONObject(walletJson);
            JSONArray data = json.getJSONArray("data");

            int expectedMatchedPromoCounts = 0;
            int actualMatchedPromoCounts = 0;

            for (int i = 0; i < data.length(); i++) {

                JSONObject obj = data.getJSONObject(i);
                JSONArray walletPromotionArray = obj.getJSONArray("promotion");
//                System.out.println("Wallet promotion Array : " + walletPromotionArray);

                for (int j = 0; j < walletPromotionArray.length(); j++) {

                    // query parameters
                    BasicDBObject dbobj = new BasicDBObject();
                    dbobj.put("promotionId", walletPromotionArray.getString(j));
//                   System.out.println("Query :"+dbobj);

                    // mongo db count query
                    long countDocuments = collection.countDocuments(dbobj);

                    if (countDocuments > 0) {
                        actualMatchedPromoCounts++;
                    }
                }
                expectedMatchedPromoCounts = expectedMatchedPromoCounts + walletPromotionArray.length();
            }
            System.out.println("expected counts : " + expectedMatchedPromoCounts);
            System.out.println("actual counts : " + actualMatchedPromoCounts);

            if (actualMatchedPromoCounts == expectedMatchedPromoCounts) {
                result = "matched";
            } else {
                result = "not matched";
            }
            //mongoClient.close();
        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }

    public String validateVaultDataInVaultPromotion(String vaultData) {
        String mongoUri = getConnectionURI();
        String result = "not matched";
        try {
//            MongoClientURI uri = new MongoClientURI(mongoUri);
//            MongoClient mongoClient = new MongoClient(uri);
//            System.out.println("Mongo client connection successful");

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("vaultPromotion").withReadPreference(ReadPreference.primaryPreferred());

            // parse vault data in json
            JSONArray vaultPromos = new JSONArray(vaultData);
//            System.out.println(vaultPromos);

            int matchedPromotions = 0;

            for (int i = 0; i < vaultPromos.length(); i++) {
                JSONObject promo = vaultPromos.getJSONObject(i);

                // adding 7 days offset and parsing into proper UTC format
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Calendar c = Calendar.getInstance();
                c.setTime(sdf.parse(promo.getString("startDate")));
                c.add(Calendar.DAY_OF_MONTH, 7);
                String offsetStartDate = sdf.format(c.getTime());

                c.setTime(sdf.parse(promo.getString("endDate")));
                c.add(Calendar.DAY_OF_MONTH, 7);
                String offsetEndDate = sdf.format(c.getTime());

                System.out.println(offsetStartDate + " " + offsetEndDate);

                String startDateStr = offsetStartDate + " 00:00:00.000 UTC";
                Date startDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS ZZZ").parse(startDateStr);
                String endDateStr = offsetEndDate + " 23:59:59.999 UTC";
                Date endDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS ZZZ").parse(endDateStr);

                System.out.println(startDate + " " + endDate);
                // Parse start and end date in proper format
//                String startDateStr = promo.getString("startDate") + " 00:00:00.000 UTC";
//                Date startDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS ZZZ").parse(startDateStr);
//                String endDateStr = promo.getString("endDate") + " 00:00:00.000 UTC";
//                Date endDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS ZZZ").parse(endDateStr);

                // query parameters
                BasicDBObject dbobj = new BasicDBObject();
                dbobj.put("promotionId", promo.getString("promotionId"));
                dbobj.put("campaingId", promo.getString("campaignId"));
                dbobj.put("campaignName", promo.getString("campaignName"));
                dbobj.put("startDate", startDate);
                dbobj.put("endDate", endDate);
//                   System.out.println("Query :"+dbobj);

                // mongo db count query
                long countDocuments = collection.countDocuments(dbobj);

                if (countDocuments > 0) {
                    matchedPromotions++;
                }
            }

            System.out.println("Matched Promotions : " + matchedPromotions);

            if (matchedPromotions == vaultPromos.length()) {
                result = "matched";
            } else {
                result = "not matched";
            }
          //  mongoClient.close();
        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }


    public String validateCeltraOfferInUnknownUserOffer(String celtraCallResponse) {
        String mongoURI = getConnectionURI();
        String result = "NOT MATCHED";
        try {
            MongoClientURI uri = new MongoClientURI(mongoURI);
            MongoClient mongoClient = new MongoClient(uri);
            System.out.println("Mongo client connection successful");

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("unknownUserOffer").withReadPreference(ReadPreference.primaryPreferred());

            // parse celtra response into json array
            JSONArray celtraResponseArray = new JSONArray(celtraCallResponse);

            int matchedDidOffers = 0;

            for (int i = 0; i < celtraResponseArray.length(); i++) {
                JSONObject responseJson = celtraResponseArray.getJSONObject(i);
//                System.out.println(responseJson);
                JSONObject response = responseJson.getJSONObject("response");
                String did = responseJson.getString("did").toUpperCase();
                String nettworkId = responseJson.getString("networkId");

                // query parameters
                BasicDBObject query = new BasicDBObject();
                query.put("did", did);
                query.put("networkId", nettworkId);

                // mongo db find query
                FindIterable<Document> fi = collection.find(query);
                MongoCursor<Document> cursor = fi.iterator();
                String document = "";
                try {
                    while (cursor.hasNext()) {
                        document = cursor.next().toJson();
//                        System.out.println("Data : " + document);
                    }
                    JSONObject dbObj = new JSONObject(document);
                    JSONArray dbOffers = dbObj.getJSONArray("offers");

                    int matchedOffersCount = 0;
                    int totalceltraOffers = 1;

                    // count total offers present in celtra response
                    while (true) {
                        String offerId = "offer_id_" + totalceltraOffers;
                        try {
                            response.get(offerId);
                            totalceltraOffers++;
                        } catch (JSONException e) {
                            totalceltraOffers--;
                            break;
                        }
                    }

                    System.out.println("Total Celtra Offers : " + totalceltraOffers);

                    for (int k = 1; k <= totalceltraOffers; k++) {
                        String offerId = "offer_id_" + k;

                        for (int j = 0; j < dbOffers.length(); j++) {
                            JSONObject offersJson = (JSONObject) dbOffers.get(j);
                            if ((offersJson.get("promotion").equals(response.get(offerId)))
                                    && (offersJson.getInt("rank") == k)
                            ) {
                                matchedOffersCount++;
                                break;
                            }
                        }
                    }

                    System.out.println("Matched offers : " + matchedOffersCount);

                    if (matchedOffersCount == totalceltraOffers) {
                        matchedDidOffers++;
                    } else {
                        break;
                    }

                } finally {
                    cursor.close();
                }

            }
          //  mongoClient.close();

            System.out.println("Matched did offers : " + matchedDidOffers);
            if (matchedDidOffers == celtraResponseArray.length()) {
                result = "MATCHED";
            } else {
                result = "NOT MATCHED";
            }

        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }

    public String validateCeltraOfferInVaultPromotion(String celtraCallResponse) {
        String mongoURI = getConnectionURI();
        String result = "NOT MATCHED";
        try {
//            MongoClientURI uri = new MongoClientURI(mongoURI);
//            MongoClient mongoClient = new MongoClient(uri);
//            System.out.println("Mongo client connection successful");

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("vaultPromotion").withReadPreference(ReadPreference.primaryPreferred());

            // parse celtra response into json array
            JSONArray celtraResponseArray = new JSONArray(celtraCallResponse);

            int matchedDidOffers = 0;

            for (int i = 0; i < celtraResponseArray.length(); i++) {
                JSONObject responseJson = celtraResponseArray.getJSONObject(i);
//                System.out.println(responseJson);
                JSONObject response = responseJson.getJSONObject("response");

                int matchedOffersCount = 0;
                int totalceltraOffers = 1;

                // count total offers present in celtra response
                while (true) {
                    String offerId = "offer_id_" + totalceltraOffers;
                    try {
                        response.get(offerId);
                        totalceltraOffers++;
                    } catch (JSONException e) {
                        totalceltraOffers--;
                        break;
                    }
                }

                System.out.println("Total Celtra Offers : " + totalceltraOffers);

                for (int j = 1; j <= totalceltraOffers; j++) {
                    String offerId = "offer_id_" + j;
                    String title = "title_" + j;
                    String imageUrl = "image_" + j;
                    String promoId = response.getString(offerId);

                    // query parameters
                    BasicDBObject query = new BasicDBObject();
                    query.put("promotionId", promoId);


                    // mongo db find query
//                    FindIterable<Document> fi = collection.find(query);
//                    MongoCursor<Document> cursor = fi.iterator();
//                    String document = "";
//                    try {
//                        while (cursor.hasNext()) {
//                            document = cursor.next().toJson();
//                            System.out.println("Data : " + document);
//                        }
//                        JSONObject dbObj = new JSONObject(document);
//
//                        if ((dbObj.get("title").equals(response.get(title)))
////                         && dbObj.get("imageUrl").equals(response.get(imageUrl))
//                        ) {
//                            matchedOffersCount++;
//                        } else {
//                            break;
//                        }
//                    } finally {
//                        cursor.close();
//                    }
                    long countDocuments = collection.countDocuments(query);

                    if (countDocuments > 0) {
                        matchedOffersCount++;
                    }
                }

                System.out.println("Matched offers : " + matchedOffersCount);

                if (matchedOffersCount == totalceltraOffers) {
                    matchedDidOffers++;
                } else {
                    break;
                }

            }
       //     mongoClient.close();

            System.out.println("Matched did offers : " + matchedDidOffers);
            if (matchedDidOffers == celtraResponseArray.length()) {
                result = "MATCHED";
            } else {
                result = "NOT MATCHED";
            }

        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }

    public String validateOmniUserForWakefernUnknown(String did, String networkId) {
        String mongoUri = getConnectionURI();
        String result = "not matched";
        try {
//            MongoClientURI uri = new MongoClientURI(mongoUri);
//            MongoClient mongoClient = new MongoClient(uri);
//            System.out.println("Mongo client connection successful");

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("omniUser").withReadPreference(ReadPreference.primaryPreferred());

            // query parameters
            BasicDBObject query = new BasicDBObject();
            query.put("networkId", networkId);
            query.put("digitalId", did);
//            System.out.println("Query :" + query);

            // mongo db count query
            long documentCount = collection.countDocuments(query);

            if (documentCount > 0) {
                result = "matched";
            } else {
                result = "not matched";
            }

       //     mongoClient.close();
        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }

    public String validateVaultOffersInOmniUserOffer(String vaultResponse, String networkId, String did) {
        String mongoUri = getConnectionURI();
        String result = "not matched";
        try {
//            MongoClientURI uri = new MongoClientURI(mongoUri);
//            MongoClient mongoClient = new MongoClient(uri);
//            System.out.println("Mongo client connection successful");

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("omniUserOfferV2").withReadPreference(ReadPreference.primaryPreferred());

            // parse vault response in json array
            JSONArray vaultOffers = new JSONArray(vaultResponse);

            // query parameters
            BasicDBObject query = new BasicDBObject();
            query.put("networkId", networkId);
            query.put("digitalId", did);
//            System.out.println("Query :" + query);

            // mongo db count query
            long documentCount = collection.countDocuments(query);
            System.out.println(documentCount);

            // mongo db find query
            FindIterable<Document> fi = collection.find(query);
            MongoCursor<Document> cursor = fi.iterator();

            int matchedOffers = 0;

            try {
                while (cursor.hasNext()) {
                    String document = cursor.next().toJson();
                    JSONObject jsonDoc = new JSONObject(document);
                    JSONObject offerJson = jsonDoc.getJSONObject("omniOffer");

                    for (int i = 0; i < vaultOffers.length(); i++) {
                        JSONObject vaultOffer = vaultOffers.getJSONObject(i);

                        if (vaultOffer.get("promotion_id").equals(offerJson.get("omniOfferId"))
                                && vaultOffer.get("campaign_id").equals(offerJson.get("campaingId"))
                                && vaultOffer.get("campaign_name").equals(offerJson.get("campaignName"))
                        ) {
                            System.out.println(vaultOffer.get("promotion_id") + " " + offerJson.get("omniOfferId"));
                            matchedOffers++;
                            break;
                        }
                    }

                }
            } finally {
                cursor.close();
            }
            System.out.println("Matched offers : " + matchedOffers + " total document count : " + documentCount);
            if (documentCount == matchedOffers) {
                result = "matched";
            } else {
                result = "not matched";
            }

         //   mongoClient.close();
        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }

    public String validateCeltraOffersInOmniUserOffer(String celtraOffersresponse, String networkId, String did) {
        String mongoUri = getConnectionURI();
        String result = "not matched";
        try {
//            MongoClientURI uri = new MongoClientURI(mongoUri);
//            MongoClient mongoClient = new MongoClient(uri);
//            System.out.println("Mongo client connection successful");

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("omniUserOfferV2").withReadPreference(ReadPreference.primaryPreferred());

            JSONObject celtraOffers = new JSONObject(celtraOffersresponse);
//            System.out.println(celtraOffers);

            // query parameters
            BasicDBObject query = new BasicDBObject();
            query.put("networkId", networkId);
            query.put("digitalId", did);

            // mongo db count query
            long totalOffersInDB = collection.countDocuments(query);

            int matchedOffers = 0;
            int totalCeltraOffers = 1;

            // count total offers present in celtra response
            while (true) {
                String offerId = "offer_id_" + totalCeltraOffers;
                try {
                    celtraOffers.get(offerId);
                    totalCeltraOffers++;
                } catch (JSONException e) {
                    totalCeltraOffers--;
                    break;
                }
            }

            System.out.println("Total Celtra Offers : " + totalCeltraOffers);


            for (int i = 1; i <= totalCeltraOffers; i++) {
                String offerId = "offer_id_" + i;
                String title = "title_" + i;
                String imageUrl = "image_" + i;

                // query parameters
                BasicDBObject query1 = new BasicDBObject();
                query1.put("networkId", networkId);
                query1.put("digitalId", did);
                query1.put("omniOffer.omniOfferId", celtraOffers.getString(offerId));
                query1.put("omniOffer.creativeVariables.verbiage1", celtraOffers.getString(title));
                query1.put("omniOffer.rank", totalOffersInDB - i + 1);
//                query.put("omniOffer.creativeVariables.product_shot_url", celtraOffers.getString(imageUrl));
//                System.out.println("Query :" + query);

                // mongo db count query
                long countDocuments = collection.countDocuments(query);

                if (countDocuments > 0) {
                    matchedOffers++;
                }
            }
            System.out.println("Matched offers : " + matchedOffers);
            if (matchedOffers == totalCeltraOffers) {
                result = "matched";
            } else {
                result = "not matched";
            }

       //     mongoClient.close();
        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }

    public Boolean checkInOmniUser(String networkId, String did, String dataFeedType, String flowType) {
        String mongoUri = getConnectionURI();
        Boolean result = true;
        String subnetwork = "";
        try {
            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("omniUser").withReadPreference(ReadPreference.primaryPreferred());

            // new subnetwork changes
            if (networkId.contains(".")) {
                String networkdetails[] = networkId.split("\\.");
                networkId = networkdetails[0];
                subnetwork = networkdetails[1];
            }

            // query parameters
            BasicDBObject query = new BasicDBObject();

            if (subnetwork == "") {
                query.put("networkId", networkId);
                query.put("digitalId", did.toUpperCase());
                query.put("dataFeedType", dataFeedType);
            } else {
                query.put("networkId", networkId);
                query.put("subnetwork", subnetwork);
                query.put("digitalId", did.toUpperCase());
                query.put("dataFeedType", dataFeedType);
            }
            System.out.println("Query :" + query);

            // mongo db find query
            FindIterable<Document> fi = collection.find(query);
            MongoCursor<Document> cursor = fi.iterator();

            String document = "";

            try {
                while (cursor.hasNext()) {
                    document = cursor.next().toJson();
                }
                JSONObject jsonDoc = new JSONObject(document);
                String dbFlowType = jsonDoc.getString("flowType");
                System.out.println("DB flowType : " + dbFlowType);

                if (dbFlowType.equals(flowType)) {
                    result = true;
                } else {
                    result = false;
                }

            } catch (JSONException e) {

            } finally {
                cursor.close();
            }
        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }

    public String updateOmniUserOffer(String networkId, String digitalId, String newNetworkId) {
        String mongoUri = getConnectionURI();
        String finalResult = "";
        try {
//            MongoClientURI uri = new MongoClientURI(mongoUri);
//            MongoClient mongoClient = new MongoClient(uri);
//            System.out.println("Mongo client connection successful");

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("omniUserOfferV2").withReadPreference(ReadPreference.primaryPreferred());

            // filter parameters
            Document query = new Document();
            query.append("networkId", networkId);
            query.append("digitalId", digitalId);

            // update parameters
            Document content = new Document();
            content.append("networkId", newNetworkId);

            // set the parameters
            Document update = new Document("$set", content);

            // updateMany mongo query
            UpdateResult result = collection.updateMany(query, update);
            System.out.println("Result : " + result);

            long matchedCount = result.getMatchedCount();
            long modifiedCount = result.getModifiedCount();

            if (matchedCount == modifiedCount && matchedCount > 0) {
                finalResult = "successfull";
            } else {
                finalResult = "not successfull";
            }

        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return finalResult;
        }
    }

    public String updateUnknownUserOffer(String networkId, String digitalId, String newNetworkId) {
        String mongoUri = getConnectionURI();
        String finalResult = "";
        try {

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("unknownUserOffer").withReadPreference(ReadPreference.primaryPreferred());

            // filter parameters
            Document query = new Document();
            query.append("networkId", networkId);
            query.append("did", digitalId.toUpperCase());

            // update parameters
            Document content = new Document();
            content.append("networkId", newNetworkId);

            // set the parameters
            Document update = new Document("$set", content);

            // updateMany mongo query
            UpdateResult result = collection.updateMany(query, update);
            System.out.println("Result : " + result);

            long matchedCount = result.getMatchedCount();
            long modifiedCount = result.getModifiedCount();

            if (matchedCount == modifiedCount && matchedCount > 0) {
                finalResult = "successfull";
            } else {
                finalResult = "not successfull";
            }

        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return finalResult;
        }
    }

    public String updateShopliftrUserOffer(String networkId, String identifier, String flowType, String newIdentifier) {
        String mongoUri = getConnectionURI();
        String finalResult = "";
        try {
            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("shopliftrUserOffer").withReadPreference(ReadPreference.primaryPreferred());

            // filter parameters
            Document query = new Document();
            query.put("flowType", flowType);
            query.put("networkId", networkId);
            if (flowType == "KNOWN") {
                query.put("identifier", identifier);
            } else {
                query.put("identifier", identifier.toUpperCase());
            }

            // update parameters
            Document content = new Document();
            if (flowType == "KNOWN") {
                content.append("identifier", newIdentifier);
            } else {
                content.append("identifier", newIdentifier.toUpperCase());
            }

            // set the parameters
            Document update = new Document("$set", content);

            // updateMany mongo query
            UpdateResult result = collection.updateMany(query, update);
            System.out.println("Result : " + result);

            long matchedCount = result.getMatchedCount();
            long modifiedCount = result.getModifiedCount();
            System.out.println("Matched Count : " + matchedCount + "Modified Count : " + modifiedCount);

            if (matchedCount == modifiedCount && matchedCount > 0) {
                finalResult = "successfull";
            } else {
                finalResult = "not successfull";
            }

        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return finalResult;
        }
    }

    public String getPromotions(String networkId,int hrim) {
        String mongoUri = getConnectionURI();
        String promotions = "";
        try {
            int hri=hrim;
            MongoIterable<String> names = mongoClient.listDatabaseNames();
            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");
            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("shopliftrPromotion").withReadPreference(ReadPreference.primaryPreferred());
//            String dateStr = getCurrentDateTime(5);
//          Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS ZZZ").parse(dateStr);
//            System.out.println(date);


//             query
            BasicDBObject query = new BasicDBObject();
            query.put("retailerId", networkId);
            try {
                query.append("created",BasicDBObjectBuilder.start( "$gte",new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS\'Z\'").parse("2022-05-02T00:00:00.498Z")).get());
                //Keeping sample date "2022-04-21T00:00:00.498Z"
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            System.out.println("Qury is "+query);
            // mongo db find query
            FindIterable<Document> fi = collection.find(query);
            MongoCursor<Document> cursor = fi.iterator();
            int count = 0;

            try {
                //Check only for High resolution images
                //&& promotions.split(";").length<=20
//                while (cursor.hasNext() && count < 20 ) {
//                    String json = cursor.next().toJson();
//                    JSONObject document = new JSONObject(json);
//                    promotions += document.get("_id").toString() + ";";
//                    count++;
//                }

                int highResolutionCount=1;
                System.out.println(promotions.split(";").length);
                while (cursor.hasNext() && promotions.split(";").length<20 ) {
                    System.out.println(promotions.split(";").length);
                    String json = cursor.next().toJson();
                    JSONObject document = new JSONObject(json);
                    System.out.println(document.getJSONObject("chain").getString("imageUrl").equalsIgnoreCase(""));
                    if( !(document.getJSONObject("chain").getString("imageUrl").equalsIgnoreCase("")) && highResolutionCount<=hri)
                    {
                        promotions += document.get("_id").toString() + ";";
                        highResolutionCount++;
                        count ++;
                    }
                    else if( document.getJSONObject("chain").getString("imageUrl").equalsIgnoreCase("")){
                        promotions += document.get("_id").toString() + ";" ;
                        count ++;
                    }

                }

            }
            catch (Exception e){
                e.printStackTrace();
            }finally {
                cursor.close();
            }
//            System.out.println("Promotions : " + promotions);
      //      mongoClient.close();

        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return promotions.substring(0, promotions.length() - 1);
        }
    }
    public boolean validatehashIdDB(String networkId, String did, String cid, String flowType, String inputDidhash) {
        String mongoUri = getConnectionURI();
        String hash = "";
        try {


            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("shopliftrUserOffer").withReadPreference(ReadPreference.primaryPreferred());

            // query parameters
            BasicDBObject query = new BasicDBObject();
            if (flowType == "KNOWN" || flowType == "TV_KNOWN") {
                query.put("identifier", cid);
            } else {
                query.put("identifier", did);
            }
            query.put("flowType", flowType);
            query.put("networkId", networkId);
            System.out.println(query);

            // mongo db find query
            FindIterable<Document> fi = collection.find(query);
            MongoCursor<Document> cursor = fi.iterator();

            try {
                while (cursor.hasNext()) {
                    String data = cursor.next().toJson();
                    System.out.println("Data Fetched from DB is" + data);
                    JSONObject json = new JSONObject(data);
                    //Getting Json Array object for hashes
                    JSONArray result = json.getJSONArray("hashes");

                    for(int k=0;k<result.length();k++){

                        JSONObject hashArray = result.getJSONObject(k);
                        hash = hashArray.getString("hash");
                        if(hash==inputDidhash) {
                            System.out.println("Hash ID: " + hash);
                            return true;
                        }
                    }

                }

            } finally {
                cursor.close();
            }
      //      mongoClient.close();
        } catch (Exception e) {
            System.out.println(e.getCause());
            return  false;
        } finally {
            return true;
        }
    }
    public String deleteOmniuser(String networkId, String hashIDfromDB, String flowType) {
        // String mongoUri = getConnectionURI();
        String hash = "";
        try {
            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("omniUser").withReadPreference(ReadPreference.primaryPreferred());

            // query parameters
            BasicDBObject query = new BasicDBObject();
            query.put("hash", hashIDfromDB);
            if(flowType=="TV_KNOWN" ||flowType=="TV_UNKNOWN"){
                query.put("flowType", flowType.replace("TV_",""));
            }else{
                query.put("flowType", flowType);
            }
            if(networkId.contains(".")) {
                query.put("networkId", networkId.split(Pattern.quote("."))[0]);
                query.put("subnetwork",networkId.split(Pattern.quote("."))[1]);
            }else {
                query.put("networkId", networkId);
            }
//            query.put("networkId", networkId);
            System.out.println("QUERY USED: " + query);

            DeleteResult deleteResult = collection.deleteOne(query);

            System.out.println("No. of documents deleted : " + deleteResult.getDeletedCount());
            // mongo db find query
            DeleteResult deleteOmniResult = collection.deleteMany(query);

//            mongoClient.close();
        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return hash;
        }
    }



    //flag is to represent if hash exist in document or not
    public boolean omniUserStatus(String networkId,String flowType, String id) {
        boolean status = false;
        FindIterable<Document> fi = null;
        MongoCursor<Document> cursor=null;
        Date updatedDate;
        Document document=null;
        long count = 0;
        try {
            MongoIterable<String> names = mongoClient.listDatabaseNames();
            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");
            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("omniUser").withReadPreference(ReadPreference.primaryPreferred());
            // query parameters
            BasicDBObject query = new BasicDBObject();
            if(networkId.contains(".")) {
                query.put("networkId", networkId.split(".")[0]);
                query.put("suubnetwork",networkId.split(".")[1]);
            }else {
                query.put("networkId", networkId);
            }
            //
            //matches("[a-zA-Z]+")
            if (!id.contains("-")) {
                query.put("fscId", id);
            } else {
                query.put("digitalId", id);
            }
            if (flowType == "TV_KNOWN" || flowType == "TV_UNKNOWN") {
                query.put("flowType", flowType.replace("TV_", ""));
            } else {
                query.put("flowType", flowType);
            }
            System.out.println("QUERY USED: " + query);
            count = collection.countDocuments(query);

            if (count > 0) {
                String hashIs;
                fi = collection.find(query);
                if (fi!=null)
                     cursor = fi.iterator();
                //fetch hash in response with collection documents
                while (cursor.hasNext()) {
                 document=cursor.next();
                    hashIs = document.getString("hash");
                   updatedDate =document.getDate("updated");
                   System.out.println("Hash is "+hashIs+" Update date is "+updatedDate);

                   if(hashIs!=null){
                       status= true;
                   }
                }
//                status = true;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            status=false;
        }
    return status;

    }

    public boolean removeHashFromOmniUser(String networkId,String flowType, String id) {
        boolean status = false;
        FindIterable<Document> fi = null;
        MongoCursor<Document> cursor=null;
        Date updatedDate;
        Document document=null;
        long count = 0;
        try {
            MongoIterable<String> names = mongoClient.listDatabaseNames();
            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");
            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("omniUser").withReadPreference(ReadPreference.primaryPreferred());
            // query parameters
            BasicDBObject query = new BasicDBObject();
//            query.put("networkId", networkId);
            if(networkId.contains(".")) {
                query.put("networkId", networkId.split(Pattern.quote("."))[0]);
                query.put("subnetwork",networkId.split(Pattern.quote("."))[1]);
            }else {
                query.put("networkId", networkId);
            }
            //
            //matches("[a-zA-Z]+")
            if (!id.contains("-")) {
                query.put("fscId", id);
            } else {
                query.put("digitalId", id);
            }
            if (flowType == "TV_KNOWN" || flowType == "TV_UNKNOWN") {
                query.put("flowType", flowType.replace("TV_", ""));
            } else {
                query.put("flowType", flowType);
            }
            System.out.println("QUERY USED: " + query);
            count = collection.countDocuments(query);
                //collection.findOneAndUpdate();
            System.out.println("Document count is "+count);

            String hashIs = null;
            if (count > 0) {


                collection.updateOne(Filters.eq("digitalId", id), Updates.set("hash", ""));
                collection.updateOne(Filters.eq("digitalId", id), Updates.set("mfdTvPassworksCouponId", ""));
                fi = collection.find(query);
                if (fi!=null)
                    cursor = fi.iterator();
                //fetch hash in response with collection documents
                while (cursor.hasNext()) {
                    document=cursor.next();

                     hashIs = document.getString("hash");
                    //updatedDate =document.getDate("updated");
                    System.out.println("Hash is ---"+hashIs);

                }
                if(hashIs.isEmpty()) {
                    status = true;
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            status=false;
        }
        return status;

    }


    public  int sendGET(String url)  {
        int responseCode=0;
        try {

            URI uri=new URI(url);
            java.awt.Desktop.getDesktop().browse(uri);
            Thread.sleep(5*1000);
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("GET");
             responseCode = con.getResponseCode();
            System.out.println("GET Response Code :: " + responseCode);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return responseCode;


    }


}




