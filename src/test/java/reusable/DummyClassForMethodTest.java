package reusable;

import com.mongodb.DB;
import com.opencsv.exceptions.CsvException;
import cucumber.api.java.ca.Cal;
import org.apache.commons.collections.map.HashedMap;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;
import com.fasterxml.jackson.databind.*;


public class DummyClassForMethodTest {
    public static void main(String args[]) throws IOException, CsvException, ParseException {

//    DBConnection dbc=new DBConnection();
//
//        System.out.println(dbc.getPromotions("24",20));
//
//        CSVToJsonFileFormatter formatter=new CSVToJsonFileFormatter();
//
//       System.out.println(formatter.formatJSONhpo("Meijer_SampleWalletFile_HPO.csv"));

        String body="{\"data\":[{\"score\":[\"12\",\" 11\",\" 10\",\" 9\",\" 8\",\" 7\",\" 6\",\" 5\",\" 4\",\" 3\",\" 2\",\" 1\"],\"cchid\":\"USA-1500-14016094371500\",\"ad\":\"HPO2022\",\"external_id\":[\"https://catalina.imgix.net/thirdparty/country_code=USA/network=24/system=ETL/4103(copy1).png\",\" https://catalina.imgix.net/thirdparty/country_code=USA/network=24/system=ETL/2195.png\",\" https://catalina.imgix.net/thirdparty/country_code=USA/network=24/system=ETL/3465(copy1).png\",\" https://catalina.imgix.net/thirdparty/country_code=USA/network=24/system=ETL/2217.png\",\" https://catalina.imgix.net/thirdparty/country_code=USA/network=24/system=ETL/3969(copy1).png\",\" https://catalina.imgix.net/thirdparty/country_code=USA/network=24/system=ETL/5831(copy1).png\",\" https://catalina.imgix.net/thirdparty/country_code=USA/network=24/system=ETL/2683.png\",\" https://catalina.imgix.net/thirdparty/country_code=USA/network=24/system=ETL/3776(copy1).png\",\" https://catalina.imgix.net/thirdparty/country_code=USA/network=24/system=ETL/3212(copy1).png\",\" https://catalina.imgix.net/thirdparty/country_code=USA/network=24/system=ETL/2170(copy5).png\",\" https://catalina.imgix.net/thirdparty/country_code=USA/network=24/system=ETL/2597.png\",\" https://catalina.imgix.net/thirdparty/country_code=USA/network=24/system=ETL/3887(copy1).png\"]}]}";
        DBConnection dbc=new DBConnection();
        System.out.println(dbc.validateWalletFileContentHpo(body,"24","KNOWN"));
//
//            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS ZZZ");
//            SimpleDateFormat formater=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS ZZZ");
//            LocalDateTime datetime = LocalDateTime.now(ZoneOffset.UTC);
////            String aftersubtraction = datetime.format(formatter);
////            System.out.println(aftersubtraction);
//        Calendar cal =Calendar.getInstance();
//        cal.setTimeZone(TimeZone.getTimeZone("UTC"));
//        System.out.println(cal.getTime());
//            System.out.println(formater.format(new Date()));
////        Date datetime = Date.now(ZoneOffset.UTC);
//        Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS ZZZ").parse(formater.format(new Date(Long.parseLong("1650931200000"))));
//        System.out.println(date);
//

//        System.out.println(System.getProperties().getProperty("java.class.path"));

//        Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS ZZZ").parse(dateStr);
//        System.out.println(date);

//        WriteToCSVFile wtc= new WriteToCSVFile();
//        wtc.writeToWalletShoplifter2("USA-23-1010","CirpTV_MeijerKnown_Sample_Wallet_Test1.csv","Promotion",1,2);

        //String data="{"data":[{"cid" : "USA-0024-100000187","ad" : "a_24_2020-12-10","promotion" : ["a69f24ad-6b2f-45e6-92d0-52f35c9c0be8","89027ac9-948c-4411-a1e7-5f24d4d1f3a0","0acca6e3-7665-4ca7-8145-ae9f5dbf9ab1","bda2fae4-5aba-45bd-b636-f45c1eb4f8c4","e72c3d8d-2113-4a73-bfbf-7ac21cd9e106","da7fe088-7388-4204-a96f-594ac69a3e5a","6d5b1ebf-aecf-4d09-9c8c-9d3e3b605552","4b9d933a-0978-47f8-b28b-27656e8ec833","e695505f-4bd7-4dc7-8f4a-71a0aab52e54","b3134dc0-301f-4761-8516-61cee1c36120","4d38bfb2-15d5-4a2c-8b73-a0ee8c102024","7e3f98ab-dbbb-40ff-9c85-5d1ba6186345","24bbeaf7-c9c4-4179-8796-eb8a1c4de390","e24c2591-492b-4fed-8d0f-3a897776a690","32b15200-f7cc-40ea-b883-1870d9e3be45","2dabab55-a314-40c9-b4b9-5d0cfce91ec7","c48cf599-f64d-4018-a015-1876f3282604","88483641-3599-4272-83a5-4d939661aaf4","ab2d82ab-e6c8-40cc-b843-c0995e17726a","8f4742ac-7756-427d-b6fe-cd2a595a8e4f"],"score" : ["1.0","2.0","4.0","5.0","6.0","7.0","8.0","9.0","10.0","11.0","12.0","13.0","14.0","15.0","16.0","17.0","18.0","19.0","20.0","3.0"],"sponsored" : ["1","0","0","0","1","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"]}]}\"";

//
//        WriteToCSVFile wtc= new WriteToCSVFile();
//        String promotion="38f999a5-62a2-4d3b-9909-eaedcea32881;28db8d87-215a-4f65-af11-d7df24599d30;91aef31b-aa49-4526-bd2f-04ecaea62c5c;b0b85972-f27b-478c-8f32-60b31be75518;34078da5-cfd4-437c-8761-35bec4b57b10;6de07108-9581-41ac-88c5-6f9fb936759f;433d9e49-4f53-41f8-9b31-f36a89be9012;ad3cbfcf-6fa6-496f-97f9-6296545f7c48;3fa5ad40-0768-49e1-9355-159ea7995461;4b435a23-0cc7-460e-b23c-3096e4b805da;a978732a-63da-4709-b789-626bc1c71580;54c38e07-ed95-44b1-bf97-346abb71d91d;1d13dc8e-50d3-4e51-88ee-d299e8d0ca80;f67fccfb-fec2-4416-b207-9c6787d417a3;2d088f27-eb94-4e11-9e97-ca20ae2141a6;4d38bfb2-15d5-4a2c-8b73-a0ee8c102024;24bbeaf7-c9c4-4179-8796-eb8a1c4de390;bcc36a53-2176-46d4-afa4-7c052fd62a1a;4880029d-c5fc-4b2c-9a31-2d7607c6c104;960262df-33ab-4474-8734-2f6bea435a2f";
////        wtc.writeToWallet("Giant_cirpUnknown_Sample_Wallet.csv",promotion);
//        wtc.writeToWalletShoplifter("USA-0024-100000188","Meijer_Sample_Wallet.csv",
//                "38f999a5-62a2-4d3b-9909-eaedcea32881;28db8d87-215a-4f65-af11-d7df24599d30;91aef31b-aa49-4526-bd2f-04ecaea62c5c;b0b85972-f27b-478c-8f32-60b31be75518;34078da5-cfd4-437c-8761-35bec4b57b10;6de07108-9581-41ac-88c5-6f9fb936759f;433d9e49-4f53-41f8-9b31-f36a89be9012;ad3cbfcf-6fa6-496f-97f9-6296545f7c48;3fa5ad40-0768-49e1-9355-159ea7995461;4b435a23-0cc7-460e-b23c-3096e4b805da;a978732a-63da-4709-b789-626bc1c71580;54c38e07-ed95-44b1-bf97-346abb71d91d;1d13dc8e-50d3-4e51-88ee-d299e8d0ca80;f67fccfb-fec2-4416-b207-9c6787d417a3;2d088f27-eb94-4e11-9e97-ca20ae2141a6;4d38bfb2-15d5-4a2c-8b73-a0ee8c102024;24bbeaf7-c9c4-4179-8796-eb8a1c4de390;bcc36a53-2176-46d4-afa4-7c052fd62a1a;4880029d-c5fc-4b2c-9a31-2d7607c6c104;960262df-33ab-4474-8734-2f6bea435a2f","2","S"
//
//        );
//        Map<String, Object> map=new HashMap<>();
//        CSVHelper csvHelper=new CSVHelper();
//        String[] identifiers={"fade6de7-b923-4a11-881d-d9dca8016fe1","577c882b-b7e2-4933-b112-9307ed5db111","a3d8110b-f2ed-4919-911b-e8241812dfbb",
//                "d385588a-b77b-4aef-9272-2653d9590a61","90d59354-15ab-4e7f-bd5f-3e6872702787"};
//        String promotion="38f999a5-62a2-4d3b-9909-eaedcea32881;28db8d87-215a-4f65-af11-d7df24599d30;91aef31b-aa49-4526-bd2f-04ecaea62c5c;b0b85972-f27b-478c-8f32-60b31be75518;34078da5-cfd4-437c-8761-35bec4b57b10;6de07108-9581-41ac-88c5-6f9fb936759f;433d9e49-4f53-41f8-9b31-f36a89be9012;ad3cbfcf-6fa6-496f-97f9-6296545f7c48;3fa5ad40-0768-49e1-9355-159ea7995461;4b435a23-0cc7-460e-b23c-3096e4b805da;a978732a-63da-4709-b789-626bc1c71580;54c38e07-ed95-44b1-bf97-346abb71d91d;1d13dc8e-50d3-4e51-88ee-d299e8d0ca80;f67fccfb-fec2-4416-b207-9c6787d417a3;2d088f27-eb94-4e11-9e97-ca20ae2141a6;4d38bfb2-15d5-4a2c-8b73-a0ee8c102024;24bbeaf7-c9c4-4179-8796-eb8a1c4de390;bcc36a53-2176-46d4-afa4-7c052fd62a1a;4880029d-c5fc-4b2c-9a31-2d7607c6c104;960262df-33ab-4474-8734-2f6bea435a2f";
//        int[] sponsored1={1,2,3,4,5};
//        int[] sponsored2={3,4,6,8,9};
//        csvHelper.createWalletFileWithMultipleLine(identifiers,"Test_Giant6",promotion,sponsored1,sponsored2);
//    WriteToCSVFile wtc=new WriteToCSVFile();
//    wtc.writeToWalletShoplifter("Meijer_Sample_Wallet_TV.csv","USA-0024-100000187",
//            "38f999a5-62a2-4d3b-9909-eaedcea32881;28db8d87-215a-4f65-af11-d7df24599d30;91aef31b-aa49-4526-bd2f-04ecaea62c5c;b0b85972-f27b-478c-8f32-60b31be75518;34078da5-cfd4-437c-8761-35bec4b57b10;6de07108-9581-41ac-88c5-6f9fb936759f;433d9e49-4f53-41f8-9b31-f36a89be9012;ad3cbfcf-6fa6-496f-97f9-6296545f7c48;3fa5ad40-0768-49e1-9355-159ea7995461;4b435a23-0cc7-460e-b23c-3096e4b805da;a978732a-63da-4709-b789-626bc1c71580;54c38e07-ed95-44b1-bf97-346abb71d91d;1d13dc8e-50d3-4e51-88ee-d299e8d0ca80;f67fccfb-fec2-4416-b207-9c6787d417a3;2d088f27-eb94-4e11-9e97-ca20ae2141a6;4d38bfb2-15d5-4a2c-8b73-a0ee8c102024;24bbeaf7-c9c4-4179-8796-eb8a1c4de390;bcc36a53-2176-46d4-afa4-7c052fd62a1a;4880029d-c5fc-4b2c-9a31-2d7607c6c104;960262df-33ab-4474-8734-2f6bea435a2f",1,3
//        );
    }
}
