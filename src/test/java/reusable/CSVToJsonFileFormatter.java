package reusable;

import com.redis.S;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class CSVToJsonFileFormatter {

    public JSONObject formatJSON(String filename) throws IOException {
        String classpathStr = System.getProperty("user.dir");
        String csvFile = classpathStr + "\\src\\test\\java\\sampletest\\dataFiles\\" + filename;
        BufferedReader br = null;
        String line = "";
        String csvSplitBy = ",";
        int count = 0;
        String csvHeader [] = new String[4];
        String data = "{\"data\":[";

        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                if (count == 0 ) {
                    csvHeader = line.split(csvSplitBy);
                }
                else {
                    String csvData [] = line.split(csvSplitBy);
                    String promotions [] = csvData[2].split(";");
                    String scores [] = csvData[3].split(";");
                    //String sponsored [] = csvData[4].split(";");

                    String promotionJson = "[";
                    for(int i = 0; i < promotions.length-1; i++) {
                        promotionJson = promotionJson + "\"" + promotions[i] + "\",";
                    }
                    promotionJson = promotionJson + "\"" + promotions[promotions.length - 1] + "\"]";



                    String scoreJson = "[";
                    for(int i = 0; i < scores.length-1; i++) {
                        scoreJson = scoreJson + "\"" + scores[i] + "\",";
                    }
                    scoreJson = scoreJson + "\"" + scores[scores.length - 1] + "\"]";


//                    String sponsoredJson = "[";
//                    for(int i = 0; i < sponsored.length-1; i++) {
//                        sponsoredJson = sponsoredJson + "\"" + sponsored[i] + "\",";
//                    }
//                    sponsoredJson = sponsoredJson + "\"" + sponsored[sponsored.length - 1] + "\"]";




                    String jsonData = "{\"" + csvHeader[0] + "\" : " + "\"" + csvData[0] + "\","
                            + "\"" + csvHeader[1] + "\" : " + "\"" + csvData[1] + "\","
                            + "\"" + csvHeader[2] + "\" : " + promotionJson + ","
                            + "\"" + csvHeader[3] + "\" : " + scoreJson
//                            + "\"" + csvHeader[4] + "\" : " + sponsoredJson
                            + "},";
                    data = data + jsonData;
                }
                count++;
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        data = data.substring(0, data.length() - 1) + "]}";
        System.out.println(data);
        JSONObject json = new JSONObject(data);
        return json;

    }

    public JSONObject formatJSONShoplifter(String filename) throws IOException {
        String classpathStr = System.getProperty("user.dir");
        String csvFile = classpathStr + "\\src\\test\\java\\sampletest\\dataFiles\\" + filename;
        BufferedReader br = null;
        String line = "";
        String csvSplitBy = ",";
        int count = 0;
        String csvHeader [] = new String[4];
        String data = "{\"data\":[";

        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                if (count == 0 ) {
                    csvHeader = line.split(csvSplitBy);
                }
                else {
                    String csvData [] = line.split(csvSplitBy);
                    String promotions [] = csvData[2].split(";");
                    String scores [] = csvData[3].split(";");
                    String sponsored [] = csvData[4].split(";");

                    String promotionJson = "[";
                    for(int i = 0; i < promotions.length-1; i++) {
                        promotionJson = promotionJson + "\"" + promotions[i] + "\",";
                    }
                    promotionJson = promotionJson + "\"" + promotions[promotions.length - 1] + "\"]";



                    String scoreJson = "[";
                    for(int i = 0; i < scores.length-1; i++) {
                        scoreJson = scoreJson + "\"" + scores[i] + "\",";
                    }
                    scoreJson = scoreJson + "\"" + scores[scores.length - 1] + "\"]";


                    String sponsoredJson = "[";
                    for(int i = 0; i < sponsored.length-1; i++) {
                        sponsoredJson = sponsoredJson + "\"" + sponsored[i] + "\",";
                    }
                    sponsoredJson = sponsoredJson + "\"" + sponsored[sponsored.length - 1] + "\"]";




                    String jsonData = "{\"" + csvHeader[0] + "\" : " + "\"" + csvData[0] + "\","
                            + "\"" + csvHeader[1] + "\" : " + "\"" + csvData[1] + "\","
                            + "\"" + csvHeader[2] + "\" : " + promotionJson + ","
                            + "\"" + csvHeader[3] + "\" : " + scoreJson + ","
                            + "\"" + csvHeader[4] + "\" : " + sponsoredJson
                            + "},";
                    data = data + jsonData;
                }
                count++;
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        data = data.substring(0, data.length() - 1) + "]}";
        System.out.println(data);
        JSONObject json = new JSONObject(data);
        return json;

    }

    public JSONObject formatJSONhpo(String filename) throws IOException {
        String classpathStr = System.getProperty("user.dir");
        String csvFile = classpathStr + "\\src\\test\\java\\sampletest\\dataFiles\\" + filename;
        BufferedReader br = null;
        String line = "";
        String csvSplitBy = ",";
        int count = 0;
        String csvHeader [] = new String[4];
        String data = "{\"data\":[";

        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                if (count == 0 ) {
                    csvHeader = line.split(csvSplitBy);
                }
                else {
                    String csvData [] = line.split(csvSplitBy);
                    String promotions [] = csvData[2].split(";");
                    String scores [] = csvData[3].split(";");


                    String promotionJson = "[";
                    for(int i = 0; i < promotions.length-1; i++) {
                        promotionJson = promotionJson + "\"" + promotions[i] + "\",";
                    }
                    promotionJson = promotionJson + "\"" + promotions[promotions.length - 1] + "\"]";



                    String scoreJson = "[";
                    for(int i = 0; i < scores.length-1; i++) {
                        scoreJson = scoreJson + "\"" + scores[i] + "\",";
                    }
                    scoreJson = scoreJson + "\"" + scores[scores.length - 1] + "\"]";

//
//                    String sponsoredJson = "[";
//                    for(int i = 0; i < sponsored.length-1; i++) {
//                        sponsoredJson = sponsoredJson + "\"" + sponsored[i] + "\",";
//                    }
//                    sponsoredJson = sponsoredJson + "\"" + sponsored[sponsored.length - 1] + "\"]";




                    String jsonData = "{\"" + csvHeader[0] + "\" : " + "\"" + csvData[0] + "\","
                            + "\"" + csvHeader[1] + "\" : " + "\"" + csvData[1] + "\","
                            + "\"" + csvHeader[2] + "\" : " + promotionJson + ","
                            + "\"" + csvHeader[3] + "\" : " + scoreJson

                            + "},";
                    data = data + jsonData;
                }
                count++;
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        data = data.substring(0, data.length() - 1) + "]}";
        System.out.println(data);
        JSONObject json = new JSONObject(data);
        return json;

    }
}
