package reusable;

import com.google.gson.JsonArray;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.ReadPreference;
import com.mongodb.client.*;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Map;
import java.util.Properties;

public class DBConnectionSkux {

    private String getConnectionURI() {
        ReadPropertyfiles rpf = new ReadPropertyfiles();
        Properties properties = rpf.prop();

        String user = properties.getProperty("user");
        String database = properties.getProperty("offerDatabase");
        String strPassword = properties.getProperty("password");
        String host = properties.getProperty("host");
        String authMechanism = properties.getProperty("authMechanism");
        String replicaSet = properties.getProperty("replicaSet");

        String mongoUri = "mongodb://" + user + ":" + strPassword + "@" + host + "/" + database + "&authMechanism=" + authMechanism + "&replicaSet=" + replicaSet + "&readPreference=primary&connectTimeoutms=6000&socketTimeoutMS=300000?tls=true";

        return mongoUri;
    }

    public String validateOffer(String offerId) {
        String mongoUri = getConnectionURI();
        String result = "not matched";
        // string uri connection
        try {
            MongoClientURI uri = new MongoClientURI(mongoUri);
            MongoClient mongoClient = new MongoClient(uri);
            System.out.println("Mongo client connection successful");

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("offer-validation");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("offers").withReadPreference(ReadPreference.primaryPreferred());

//            System.out.println(offerDetails);
//            JSONObject offersJson = new JSONObject(offerDetails);

            // query parameters
            BasicDBObject query = new BasicDBObject();
            query.put("_id", new ObjectId(offerId));
//            query.put("definition.title", offersJson.getString("title"));
//            query.put("definition.retailerId", offersJson.getString("retailerId"));
//            query.put("definition.workflowType", offersJson.getString("workflowType"));

            // count query
            long count = collection.countDocuments(query);
            System.out.println(count);

            if (count == 1) {
                result = "Matched";
            }
            mongoClient.close();
        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }

    public String getOfferId(String title, String retailerId, String workflowType) {
        String mongoUri = getConnectionURI();
        String offerId = "";
        // string uri connection
        try {
            MongoClientURI uri = new MongoClientURI(mongoUri);
            MongoClient mongoClient = new MongoClient(uri);
            System.out.println("Mongo client connection successful");

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("offer-validation");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("offers").withReadPreference(ReadPreference.primaryPreferred());

            // query parameters
            BasicDBObject query = new BasicDBObject();
            query.put("definition.title", title);
            query.put("definition.retailerId", retailerId);
            query.put("definition.workflowType", workflowType);

            System.out.println("Fetching all documents from the collection");
            FindIterable<Document> fi = collection.find(query);
            MongoCursor<Document> cursor = fi.iterator();

            // validate data in response with collection documents
            try {
                while (cursor.hasNext()) {
                    String document = cursor.next().toJson();
                    System.out.println("Document : " + document);
                    JSONObject offerJson = new JSONObject(document);
                    JSONObject objectId = offerJson.getJSONObject("_id");
                    offerId = objectId.getString("$oid");
                }
            } finally {
                cursor.close();
            }

            System.out.println("Offer Id : " + offerId);
            mongoClient.close();

        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return offerId;
        }
    }

    public String checkCidOfferMonitor(String did, String offerId, String flowType) {
        String mongoUri = getConnectionURI();
        String result = "not matched";
        // string uri connection
        try {
            MongoClientURI uri = new MongoClientURI(mongoUri);
            MongoClient mongoClient = new MongoClient(uri);
            System.out.println("Mongo client connection successful");

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("offer-validation");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("cid-offer-monitors").withReadPreference(ReadPreference.primaryPreferred());

//            String validationTarget = "";
//            if (flowType == "instore") {
//                validationTarget = "skux-validation-target";
//            } else {
//                validationTarget = "ecomm-validation-target";
//            }

            // query parameters
            BasicDBObject query = new BasicDBObject();
            query.put("digitalId", did);
            query.put("offerId", offerId);
//            query.put("type", flowType);

            System.out.println("Query is "+query);
            long count = collection.countDocuments(query);

            System.out.println("Fetching all documents from the collection");
            FindIterable<Document> fi = collection.find(query);
            MongoCursor<Document> cursor = fi.iterator();

            String cidOfferMonitorId = "";
            Boolean disabled = false;

            // validate data in response with collection documents
            try {
                while (cursor.hasNext()) {
                    String json = cursor.next().toJson();
                    System.out.println("Check for the values" + json);
                    JSONObject dbObj = new JSONObject(json);
                    JSONObject id = dbObj.getJSONObject("_id");
                    cidOfferMonitorId = id.get("$oid").toString();
                    disabled = dbObj.getBoolean("disabled");
                    System.out.println("Monitor Id : " + cidOfferMonitorId + " and disabled : " + disabled);
                }
            } finally {
                cursor.close();
            }

            if (count == 1) {
                result = "cid-offer monitor found with id : " + cidOfferMonitorId + " and disabled : " + disabled;
            } else if (count > 1) {
                result = "multiple cid-offer monitor found";
            } else {
                result = "cid-offer monitor not found";
            }

            mongoClient.close();

        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }

    public String checkFundingResults(String cidOfferMonitorId) {
        String mongoUri = getConnectionURI();
        String result = "not found";
        // string uri connection
        try {
            MongoClientURI uri = new MongoClientURI(mongoUri);
            MongoClient mongoClient = new MongoClient(uri);
            System.out.println("Mongo client connection successful");

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("offer-validation");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("funding-results").withReadPreference(ReadPreference.primaryPreferred());

            // query parameters
            BasicDBObject query = new BasicDBObject();
            query.put("cidOfferMonitorId", cidOfferMonitorId);

            // document count with particular cidOfferMonitorId
            long count = collection.countDocuments(query);

            System.out.println("Fetching all documents from the collection");
            FindIterable<Document> fi = collection.find(query);
            MongoCursor<Document> cursor = fi.iterator();

            // validate data in response with collection documents
            try {
                while (cursor.hasNext()) {
                    String json = cursor.next().toJson();
                    System.out.println("Document : " + json);
                }
            } finally {
                cursor.close();
            }

            if (count == 1) {
                result = "cid-offer monitor found in funding results";
            } else {
                result = "cid-offer monitor not found in funding results";
            }

            mongoClient.close();

        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }

    public String getExternalPromotionId(String offerId) {
        String mongoUri = getConnectionURI();
        String externalPromotionId = "";
        // string uri connection
        try {
            MongoClientURI uri = new MongoClientURI(mongoUri);
            MongoClient mongoClient = new MongoClient(uri);
            System.out.println("Mongo client connection successful");

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("offer-validation");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("offers").withReadPreference(ReadPreference.primaryPreferred());

            // query parameters
            BasicDBObject query = new BasicDBObject();
            query.put("_id", new ObjectId(offerId));

            System.out.println("Fetching all documents from the collection");
            FindIterable<Document> fi = collection.find(query);
            MongoCursor<Document> cursor = fi.iterator();

            // validate data in response with collection documents
            try {
                while (cursor.hasNext()) {
                    String document = cursor.next().toJson();
                    System.out.println("Document : " + document);
                    JSONObject offerJson = new JSONObject(document);
                    JSONObject obj = offerJson.getJSONObject("definition");
                    externalPromotionId = obj.getString("externalPromotionId");
                }
            } finally {
                cursor.close();
            }

            mongoClient.close();

        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return externalPromotionId;
        }
    }

    public String validateOfferStatus(String offerId) {
        String mongoUri = getConnectionURI();
        String status = "FAILED";
        // string uri connection
        try {
            MongoClientURI uri = new MongoClientURI(mongoUri);
            MongoClient mongoClient = new MongoClient(uri);
            System.out.println("Mongo client connection successful");

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("offer-validation");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("offer-published-event").withReadPreference(ReadPreference.primaryPreferred());

            // query parameters
            BasicDBObject query = new BasicDBObject();
            query.put("offerId", offerId);

            System.out.println("Fetching all documents from the collection");
            FindIterable<Document> fi = collection.find(query);
            MongoCursor<Document> cursor = fi.iterator();

            // validate data in response with collection documents
            try {
                while (cursor.hasNext()) {
                    String document = cursor.next().toJson();
                    System.out.println("Document : " + document);
                    JSONObject offerJson = new JSONObject(document);
                    status = offerJson.getString("status");
                }
            } finally {
                cursor.close();
            }

            mongoClient.close();

        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return status;
        }
    }

    public String checkRedeemResults(String cidOfferMonitorId) {
        String mongoUri = getConnectionURI();
        String result = "cidOfferMonitorId is not found in redeem results";
        // string uri connection
        try {
            MongoClientURI uri = new MongoClientURI(mongoUri);
            MongoClient mongoClient = new MongoClient(uri);
            System.out.println("Mongo client connection successful");

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("offer-validation");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("redeem-results").withReadPreference(ReadPreference.primaryPreferred());

            // query parameters
            BasicDBObject query = new BasicDBObject();
            query.put("cidOfferMonitorId", cidOfferMonitorId);

            // document count with particular cidOfferMonitorId
            long count = collection.countDocuments(query);

            if (count >= 1) {
                result = "cidOfferMonitorId is found in redeem results";
            } else {
                result = "cidOfferMonitorId is not found in redeem results";
            }

            mongoClient.close();

        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }

    public String updateCidOfferMonitor(String did, String offerId, String cid, String flowType) {
        String mongoUri = getConnectionURI();
        String finalResult = "";
        // string uri connection
        try {
            MongoClientURI uri = new MongoClientURI(mongoUri);
            MongoClient mongoClient = new MongoClient(uri);
            System.out.println("Mongo client connection successful");

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("offer-validation");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("cid-offer-monitors").withReadPreference(ReadPreference.primaryPreferred());

            // filter parameters
            Document query = new Document();
            query.put("digitalId", did);
            query.put("offerId", offerId);
            query.put("type", flowType);

            // convert to jsonArray
            JSONArray cidjson = new JSONArray(cid);

            // update parameters
            Document content = new Document();
            content.append("cids", cidjson);

            // set the parameters
            Document update = new Document("$set", content);

            // updateMany mongo query
            UpdateResult result = collection.updateMany(query, update);
            System.out.println("Result : " + result);

            long matchedCount = result.getMatchedCount();
            long modifiedCount = result.getModifiedCount();
            System.out.println("Matched Count : " + matchedCount + "Modified Count : " + modifiedCount);

            if (matchedCount == modifiedCount && matchedCount > 0) {
                finalResult = "successfull";
            } else {
                finalResult = "not successfull";
            }
            mongoClient.close();

        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return finalResult;
        }
    }

    public void deleteOffers(String title, String workflowType) {
        String mongoUri = getConnectionURI();
        // string uri connection
        try {
            MongoClientURI uri = new MongoClientURI(mongoUri);
            MongoClient mongoClient = new MongoClient(uri);
            System.out.println("Mongo client connection successful");

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("offer-validation");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("offers").withReadPreference(ReadPreference.primaryPreferred());

            // query parameters
            BasicDBObject query = new BasicDBObject();
            query.put("definition.title", title);
            query.put("definition.workflowType", workflowType);

            DeleteResult deleteResult = collection.deleteMany(query);

            System.out.println("No. of documents deleted : " +deleteResult.getDeletedCount());

            mongoClient.close();

        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
        }
    }

    public String checkBarcodeRedemption(String didOfferMonitorId, String did, String offerId, String transactionId) {
        String mongoUri = getConnectionURI();
        String result = "cidOfferMonitorId is not found in redeem results";
        // string uri connection
        try {
            MongoClientURI uri = new MongoClientURI(mongoUri);
            MongoClient mongoClient = new MongoClient(uri);
            System.out.println("Mongo client connection successful");

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("offer-validation");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("barcode-redemptions").withReadPreference(ReadPreference.primaryPreferred());

            // query parameters
            BasicDBObject query = new BasicDBObject();
            query.put("didMonitorId", didOfferMonitorId);
            query.put("digitalId", did);
            query.put("offerId", offerId);
            query.put("transactionId", transactionId);

            // document count with particular cidOfferMonitorId
            long count = collection.countDocuments(query);

            if (count == 1) {
                result = "Record found in barcode redemptions";
            } else {
                result = "Record not found in barcode redemptions";
            }

            mongoClient.close();

        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }

    public void deleteCidOfferMonitors(String title, String workflowType, String did, String retailerId) {
        String mongoUri = getConnectionURI();
        // string uri connection
        try {
            MongoClientURI uri = new MongoClientURI(mongoUri);
            MongoClient mongoClient = new MongoClient(uri);
            System.out.println("Mongo client connection successful");

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("offer-validation");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("cid-offer-monitors").withReadPreference(ReadPreference.primaryPreferred());

            String offerId = getOfferId(title, retailerId, workflowType);
            System.out.println("Offers Id : " + offerId );

            String promotionId = getExternalPromotionId(offerId);
            System.out.println("Promotions Id : " + promotionId );

            // query parameters
            BasicDBObject query = new BasicDBObject();
            query.put("offerId", promotionId);
            query.put("digitalId", did);

            DeleteResult deleteResult = collection.deleteOne(query);

            System.out.println("No. of documents deleted : " +deleteResult.getDeletedCount());

            mongoClient.close();

        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
        }
    }
}