package reusable;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ReadPropertyfiles {

    public Properties prop() {
        Properties properties;
        String classpathStr = System.getProperty("user.dir");
        String propertyFilePath = classpathStr + "\\src\\test\\java\\dbConnection.properties";

        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(propertyFilePath));
            properties = new Properties();
            try {
                properties.load(reader);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("dbConnection.properties not found at " + propertyFilePath);
        }

        return properties;

    }

}
