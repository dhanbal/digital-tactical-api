package perf

import io.gatling.core.Predef._
import scala.concurrent.duration._
import com.intuit.karate.gatling.PreDef._
class UserSim extends Simulation{
  val getSingleUser = scenario("Get user method must return 200 status").exec(karateFeature("classpath:perf/single.feature"))
  setUp(
  getSingleUser.inject(rampUsers(users=10).during(10 seconds))
    //getSingleUser.inject(constantUsersPerSec(100) during (20 seconds))).throttle(reachRps(100) in (5 seconds),holdFor(10 seconds)
  )
}
