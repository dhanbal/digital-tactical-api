Feature: Get method for single user
  Background:
    * url 'https://reqres.in'

  Scenario: Get user method must return 200 status
    Given path '/api/users/2'
    When method GET
    Then status 200