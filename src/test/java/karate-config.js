function fn() {
	var env = karate.env; // get system property 'karate.env'
	karate.log('karate.env system property was:', env);
	if (!env) {
		env = 'dev';
	}
	var config = {
		baseURL: {
		    shopliftr : 'http://shopliftr-ingestion-om-sqa.npolusk8s.catmktg.com/',
		    vaultURL : 'http://production-edge.eastus.api.catalina.com/',
		    vaultURL_1: 'http://v02-production.edge-node.45.eastus.api.catalina.com:8181/',
		    celtraCall : 'https://catalina-omni-integration-sqa.azure-api.net/',
		    solarDev : 'http://10.166.104.69:8983/solr/id_map/',
		    solarOldSQA : 'http://10.166.105.71:8983/solr/id_map/',
		    solarSQA : 'http://10.166.165.73:8983/solr/id_map/',
		    solarProd : 'http://10.176.106.75:8983/solr/id_map/',
		    skuxSqa : 'http://digital-tech-offer-validation-service-om-sqa.npolusk8s.catmktg.com/api/',
		    skuxDev : 'http://digital-tech-offer-validation-service-om-dev.npolusk8s.catmktg.com/api/',
		    skuxProd : 'http://digital-tech-offer-validation-service-om-prod.npolusk8s.catmktg.com/api/',
		},
		headers: {
			Connection: 'keep-alive',
			Content_Type: 'application/json',
			Accept_Encoding: 'gzip, deflate, br'
		},
        env: env,
		myVarName: 'someValue'
	    }
    return config;
}