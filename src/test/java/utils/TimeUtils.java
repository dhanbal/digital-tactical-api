package utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

public class TimeUtils {

    public static long getCurrentTimeMillis() {
        return System.currentTimeMillis();
    }

    public static String convertTimestampInMillisToMonthDateFormat(long timestampInMillis, String countryCode, String networkId) {
        int minutesToSubtract = 0;
        if (countryCode.equalsIgnoreCase("USA")) {
            minutesToSubtract = 300;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss z");
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.setTimeInMillis(timestampInMillis);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        System.out.println("Original Time: " + sdf.format(calendar.getTime()));
        calendar.setTimeInMillis(timestampInMillis - (minutesToSubtract * 60 * 1000));
        System.out.println("After subtracting " +minutesToSubtract+ " minutes: " +sdf.format(calendar.getTime()));

        SimpleDateFormat dateFormat = new SimpleDateFormat("M/d");
        String monthDateFormat  = dateFormat.format(calendar.getTime());
        System.out.println("monthDateFormat: " + monthDateFormat);
        return monthDateFormat;
    }

    public static void main(String[] args) {
//        TimeUtils.convertTimestampInMillisToMonthDateFormat(1931057999999L, "USA", "24");
        System.out.println(TimeUtils.getCurrentTimeMillis());
    }

}
