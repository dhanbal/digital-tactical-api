package utils;

import com.mongodb.*;
import com.mongodb.MongoClient;
import com.mongodb.client.*;
import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.bson.types.ObjectId;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;


public class MongoDBUtils {
    ReadPropertyFile readPropertyFile;
    String mongoDBConnectionString;


    public MongoDBUtils() {
        try {
            readPropertyFile = new ReadPropertyFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mongoDBConnectionString = readPropertyFile.getMongoDBConnectionString();
    }

    public String validateidomoOffers(List<String> filecontent, String networkId, String flowType) {
        String result = "";
        try {
            MongoClientURI uri = new MongoClientURI(mongoDBConnectionString);
            MongoClient mongoClient = new MongoClient(uri);
            System.out.println("Mongo client connection successful");

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("shopliftrUserOffer").withReadPreference(ReadPreference.primaryPreferred());
            MongoCollection<Document> collection1 = db.getCollection("shopliftrOffer").withReadPreference(ReadPreference.primaryPreferred());

            // query parameters
            BasicDBObject query = new BasicDBObject();

            if ("KNOWN".equalsIgnoreCase(flowType)||"TV_KNOWN".equalsIgnoreCase(flowType) ||"TVKN".equalsIgnoreCase(flowType)){
                query.put("flowType", "TV_KNOWN");
            } else {
                query.put("flowType", "TV_UNKNOWN");
            }
            String DID = filecontent.get(1).split(",")[0];
            query.put("hashes.digitalId", DID);   
            query.put("networkId", networkId);
//            if (flowType == "KNOWN" ||flowType == "TV_KNOWN" ||flowType == "TVKN") {
//                query.put("identifier", DID);
//            } else {
//                query.put("identifier", DID.toUpperCase());
//            }
                  
            System.out.println("QUERY: " + query);

            FindIterable<Document> fi = collection.find(query);
            MongoCursor<Document> cursor = fi.iterator();         

            JSONArray offers = new JSONArray();
                while (cursor.hasNext()) {
                    String data = cursor.next().toJson();                   
                    JSONObject jsonData = new JSONObject(data);
                    offers = jsonData.getJSONArray("offers");
                }
                int count = 0;  // matched offers count
                int totalidomoOffers = 5;

                for (int i=1 ; i < totalidomoOffers; i=i+5) {
                	String[] fileRow = filecontent.get(i).split(",");
                	String brand = fileRow[i+1];/////2,7,12
                	 
                    String name = fileRow[i+2];
                    String additional = fileRow[i+3];
                    String image_url = fileRow[i+4];
                    String custom_price = fileRow[i+5];                  
                for (int j = 0; j < offers.length(); j++) {
                        JSONObject offersJson = (JSONObject) offers.get(j);                       

                            // query parameters
                            BasicDBObject query1 = new BasicDBObject();
                            query1.put("promotion", offersJson.get("promotion"));                             

                            //mongo db find query
                            FindIterable<Document> fi1 = collection1.find(query1);
                            MongoCursor<Document> cursor1 = fi1.iterator();
                            String data1 = "";
                            while (cursor1.hasNext()) {
                                data1 = cursor1.next().toJson();                                
                            }
                            JSONObject jsonData = new JSONObject(data1);                           
                            System.out.println("+++++++++++++++++++Offer DB Data : +++++++++++" + jsonData.get("brand"));                           
                            if (
                            		jsonData.get("brand").equals(brand)&&                                    
                                    (jsonData.get("name").equals(name))
                                    && (jsonData.get("additional").equals(additional))
                                    && (jsonData.get("customPrice").equals(custom_price))
                                    && (jsonData.get("highResImageUrl").equals(image_url))
                                    )
                            {
                            	 result = "MATCHED";
                            	 count++;
                                break;
                            }                           
                            result = "NOT MATCHED";                            
                        }
                    }         
//
                System.out.println("Count : " +count);
//
//                if (count == totalidomoOffers) {
//                    result = "MATCHED";
//                } else {
//                    result = "NOT MATCHED";
//                }

          
//            mongoClient.close();
//        } catch (Exception e) {
//            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }
}

