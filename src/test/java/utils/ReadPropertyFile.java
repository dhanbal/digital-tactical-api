package utils;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Properties;

public class ReadPropertyFile {
    File file;
    FileInputStream fileinput;
    Properties prop;
    String rootPath;

    public ReadPropertyFile() throws IOException
    {
        rootPath = System.getProperty("user.dir");
        file = new File(rootPath+"//CircP.properties");
        fileinput = null;
        try
        {
            fileinput = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        prop = new Properties();
        prop.load(fileinput);
    }
    public Properties ReadCirpTVPropertyFile() throws IOException
    {
        rootPath = System.getProperty("user.dir");
        file = new File(rootPath+"//CirpTV.properties");
        fileinput = null;
        try
        {
            fileinput = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        prop = new Properties();
        prop.load(fileinput);
        return prop;
    }

    public String getEnvironment() {
        return prop.getProperty("environment");
    }

    public String getCeltraUrl(String urlType, String countryCode, String networkId, String DID) {
        String url = null;
        String environment = getEnvironment();

        if(environment.equalsIgnoreCase("SQA"))
        {
            if (urlType.equals("ShopliftrKnownCeltraUrl")) {
                url = prop.getProperty("SQAShopliftrKnownCeltraUrl");

            }
            else if (urlType.equals("ShopliftrUnknownCeltraUrl")) {
                url = prop.getProperty("SQAShopliftrUnknownCeltraUrl");
            }
        }

        url = url.replace("<countryCode>", countryCode);
        url = url.replace("<networkId>", networkId);
        url = url.replace("<DID>", DID);

        System.out.println("Celtra url: " +url);
        return url;
    }

    public String getMongoDBConnectionString() {
        String mongoDBconnectionString = null;
        String environment = getEnvironment();

        if(environment.equalsIgnoreCase("SQA"))
        {
            mongoDBconnectionString = prop.getProperty("SQAMongoDBConnectionString");
        }
        System.out.println(environment + " MongoDB connection String: " +mongoDBconnectionString);
        return mongoDBconnectionString;
    }

    public String getFallbackOfferCreativeInfoFromCeltraCallResponse(String countryCode, String networkId)  {
        String environment = getEnvironment();
        String fallbackOfferCreativeInfoFromCeltraCallResponse = null;
        String fallbackImageName = null;
        String retailerFolder = null;

//        if (countryCode.equals("USA") && networkId.equals("4")) {
//            fallbackImageName = "fallbackAdShoprite.png";
//            retailerFolder = "shoprite";
//        }
//        else if (countryCode.equals("USA") && networkId.equals("6")) {
//            fallbackImageName = "fallbackAdSNS.png";
//            retailerFolder = "stopshop";
//        }
//        else if (countryCode.equals("USA") && networkId.equals("24")) {
//            fallbackImageName = "fallbackAdMeijer.png";
//            retailerFolder = "meijer-shopliftr";
//        }
//        else if (countryCode.equals("USA") && (networkId.equals("6.1") || networkId.equals("6.2") || networkId.equals("8") || networkId.equals("45"))) {
//            fallbackImageName = "fallback-ad.png";
//        }
//        else if (countryCode.equals("USA") && networkId.equals("94")) {
//            fallbackImageName = "fallbackAdHannaford.png";
//            retailerFolder = "hannaford-shopliftr";
//        }

        if (environment.equalsIgnoreCase("SQA")) {
//            if (countryCode.equals("USA") && (networkId.equals("6.1") || networkId.equals("6.2") || networkId.equals("8") || networkId.equals("45"))) {
                fallbackImageName = "fallback-ad.png";
                fallbackOfferCreativeInfoFromCeltraCallResponse = prop.getProperty("newSQAFallbackOfferCreativeInfoFromCeltraCallResponse").replace("<networkId>", networkId).replace("<fallbackAd.png>", fallbackImageName);;
//            }
//            else {
//                fallbackOfferCreativeInfoFromCeltraCallResponse = prop.getProperty("SQAFallbackOfferCreativeInfoFromCeltraCallResponse").replace("<retailerFolder>", retailerFolder).replace("<fallbackAd.png>", fallbackImageName);
//            }
        }

        return fallbackOfferCreativeInfoFromCeltraCallResponse;

    }

    public static String getRetailerName(String countryCode, String networkId, String retailerName) {
//        String retailerName = null;
        if (countryCode.equals("USA") && networkId.equals("4")) {
            retailerName = "ShopRite";
        }
        if (countryCode.equals("USA") && networkId.equals("8")) {
            retailerName = "Food Lion";
        }
        else if (countryCode.equals("USA") && networkId.equals("6")) {
            retailerName = "Stop & Shop";
        }
        else if (countryCode.equals("USA") && networkId.equals("6.1")) {
            retailerName = "Giant";
        }
        else if (countryCode.equals("USA") && networkId.equals("6.2")) {
            retailerName = "Martin";
        }
        else if (countryCode.equals("USA") && networkId.equals("24")) {
            retailerName = "Meijer";
        }
        else if (countryCode.equals("USA") && networkId.equals("45")) {
            retailerName = "Weis Markets";
        }
        else if (countryCode.equals("USA") && networkId.equals("94")) {
            retailerName = "Hannaford";
        }
        System.out.println("Retailer Name is: " + retailerName);
        return retailerName;
    }

    public LinkedHashMap<String, String> getNetworkAndSubnetwork(String networkId) {
        LinkedHashMap<String, String> networkAndSubnetwork = new LinkedHashMap<>();
        String subnetwork = null;

        if (networkId.contains(".")) {
            String[] arr = networkId.split("\\.");
            networkId = arr[0];
            subnetwork = arr[1];
        }

        networkAndSubnetwork.put("network", networkId);
        networkAndSubnetwork.put("subnetwork", subnetwork);

        System.out.println("networkId: " + networkId);
        System.out.println("subnetwork: " + subnetwork);
        return networkAndSubnetwork;
    }

    public String getDealsValidityDate(String monthDateFormat) {
        String dealsValidityDate = prop.getProperty("dealsValidityDate");
        dealsValidityDate = dealsValidityDate.replace("M/d", monthDateFormat);
        System.out.println("expectedDealsValidityDate: " +dealsValidityDate);
        return dealsValidityDate;
    }

    public String getSalePricesWithRetailerCard(String retailerName) {
        System.out.println("=========Retailer Name :============== " + retailerName);
//        String retailerName = getRetailerName(countryCode, networkId, retailerName);
        String salePricesWithRetailerCard = prop.getProperty("salePricesWithRetailerCard");
        if (retailerName.equals("Weis"))
        {
            salePricesWithRetailerCard = salePricesWithRetailerCard.replace("<retailerName>", retailerName + " Markets");
        }
        else {
//        salePricesWithRetailerCard = salePricesWithRetailerCard.replace("<retailerName>", retailerName);
            salePricesWithRetailerCard = salePricesWithRetailerCard.replace("<retailerName>", retailerName);
        }
        System.out.println("salePricesWithRetailerCard: " +salePricesWithRetailerCard);
        return salePricesWithRetailerCard;
    }

    public String getRetailerWebsitePage(String countryCode, String networkId, String retailerName) {
        String retailerWebsitePage = null;
        if (countryCode.equals("USA") && networkId.equals("4")) {
            retailerWebsitePage = prop.getProperty("ShopRiteWebsitePage");
        }
        else if (countryCode.equals("USA") && networkId.equals("6")) {
            retailerWebsitePage = prop.getProperty("StopAndShopWebsitePage");
        }
        else if (countryCode.equals("USA") && networkId.equals("6.2")) {
            retailerWebsitePage = prop.getProperty("MartinWebsitePage");
        }
        else if (countryCode.equals("USA") && networkId.equals("6.1")) {
            retailerWebsitePage = prop.getProperty("GiantWebsitePage");
        }
        else if (countryCode.equals("USA") && networkId.equals("24")) {
            retailerWebsitePage = prop.getProperty("MeijerWebsitePage");
        }
        else if (countryCode.equals("USA") && networkId.equals("45")) {
            retailerWebsitePage = prop.getProperty("WeisWebsitePage");
        }
        else if (countryCode.equals("USA") && networkId.equals("94")) {
            retailerWebsitePage = prop.getProperty("HannafordWebsitePage");
        }
        else if (countryCode.equals("USA") && networkId.equals("8")) {
            retailerWebsitePage = prop.getProperty("FoodLionWebsitePage");
        }
        else if (countryCode.equals("USA") && networkId.equals("16")) {
            retailerWebsitePage = prop.getProperty("HyVeeWebsitePage");
        }
            retailerName = getRetailerName(countryCode, networkId, retailerName);
        System.out.println(retailerName + " Website Page: " + retailerWebsitePage);
        return retailerWebsitePage;
    }

    public String getCardlinkNetworkCode(String networkId) {
        String cardlinkNetworkCode = null;
        if (networkId.equals("4")) {
            cardlinkNetworkCode = "0004";
        }
        else if (networkId.equals("6")) {
            cardlinkNetworkCode = "0006";
        }
        else if (networkId.equals("8")) {
            cardlinkNetworkCode = "0008";
        }
        else if (networkId.equals("16")) {
            cardlinkNetworkCode = "0016";
        }
        else if (networkId.equals("24")) {
            cardlinkNetworkCode = "0024";
        }
        else if (networkId.equals("45")) {
            cardlinkNetworkCode = "0045";
        }
        else if (networkId.equals("94")) {
            cardlinkNetworkCode = "0094";
        }
        else if (networkId.equals("6.1")) {
            cardlinkNetworkCode = "0006";
        }
        else if (networkId.equals("6.2")) {
            cardlinkNetworkCode = "0006";
        }
        System.out.println("Cardlink Network Code is: " + cardlinkNetworkCode);
        return cardlinkNetworkCode;
    }

    public String getXApiKeyForCardlink() {
        return prop.getProperty("xApiKeyForCardlink");
    }

    public String getSQACardlinkDidToCidMappingUrl() {
        return prop.getProperty("SQACardlinkDidToCidMappingUrl");
    }

    public String getDbJdbcDriver() {
        return prop.getProperty("JdbcDriver");
    }

    public String getdbUsername_Op() {
        return prop.getProperty("DbUserName_Op");
    }

    public String getdbPassword_Op() {
        return prop.getProperty("DbPassword_Op");
    }

    public String getTestPlanId() {
        return prop.getProperty("TestPlanId");
    }

    public void setBuildID(String buildID) {
        prop.setProperty("BuildId", buildID);
    }

    public int getBuildID() {
        String build = new String();
        build = prop.getProperty("BuildId");
        return Integer.parseInt(build);
    }

    public String getBuildName() {
        System.out.println("Inside getBuildName");

        if (System.getProperty("BuildName") == null) {
            return prop.getProperty("BuildName");
        } else {
            return System.getProperty("BuildName");
        }
    }

    public String getTestPlanID() {
        if (System.getProperty("TestPlanID") == null) {
            return prop.getProperty("TestPlanId");
        } else {
            return System.getProperty("TestPlanID");
        }
    }


}

