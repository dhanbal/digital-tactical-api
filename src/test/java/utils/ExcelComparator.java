package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;

//import junit.framework.Assert;

public class ExcelComparator {
//	static String path="D:\\csv\\";
    static String file1="C:\\Users\\csauto4\\Documents\\Pavitra Automation\\idomoo-USA24TVKN20221301_1.csv";
    static String file2="C:\\Users\\csauto4\\Documents\\Pavitra Automation\\PURL-USA24TVKN20221301_1.csv";
    static String file3="C:\\Users\\csauto4\\Documents\\Pavitra Automation\\p3lang.csv";
   
    static String[] HEADERS = {"unique_id", "qr_code_url", "brand_1", "name_1", "additional_1", "image_url_1", "custom_price_1", "brand_2", "name_2", "additional_2", "image_url_2", "custom_price_2", "brand_3", "name_3", "additional_3", "image_url_3", "custom_price_3", "brand_4", "name_4", "additional_4", "image_url_4", "custom_price_4", "brand_5", "name_5", "additional_5", "image_url_5", "custom_price_5"};
    static List<String> f1 = new LinkedList<String>();
	static List<String> f2 = new LinkedList<String>();
	static LinkedHashMap<Integer, String> errorLinkedHashMap = new LinkedHashMap<>();
	 static List<String> filecontents1;
	 static List<String> filecontents2;
    public static void main(String args[]) throws FileNotFoundException, IOException
	{
    	
    	 boolean file1Order1 = isOrderOfHeadersCorrectInCsvFile(file1);
  	   if(file1Order1)
  	   {
  		   filecontents1 = readFile(file1); 
  	   }
  	   else {
  		   System.out.println("Order of headers are not matching in file : "+ file1);
  	   }
  	 boolean file2Order2 = isOrderOfHeadersCorrectInCsvFile(file2);
	   if(file2Order2)
	   {
		  filecontents2 = readFile(file2); 
	   }
	   else {
  		   System.out.println("Order of headers are not matching in file : "+ file1);
  	   }
	
 
    	Assert.assertTrue(filecontents1.equals(filecontents2)); //returns true because lists are equal
    	System.out.println("Both file contents matched");
   

		
	}
    
   public static List<String> readFile(String filename) throws FileNotFoundException
   {
	Reader in = new FileReader(file1);
    Iterable<CSVRecord> records = null;
    try {
        records = CSVFormat.Builder.create()
                .setHeader(HEADERS)
                .setSkipHeaderRecord(true)
                .build().parse(in);
//                CSVFormat.DEFAULT
//                .withHeader(HEADERS)
//                .withFirstRecordAsHeader()
//                .parse(in);
    } catch (IOException e) {
        e.printStackTrace();
    }

  
    for (CSVRecord record : records) {
	    for (String column : HEADERS) {
	        String columnValue = record.get(column);
	        f1.add(columnValue);
//	       
//	 System.out.print(column + " : " + columnValue +"\n");
	 
    }
	    
	}
//    System.out.print("F1 : " +f1);
    return f1;
      
}
   public static boolean isOrderOfHeadersCorrectInCsvFile(String filename) {
	   
	   
       boolean isOrderOfHeadersCorrectInCsvFile = true;
       String[] actualHeaders = {};
       Reader in = null;
       try {
       	 
       	 
           in = new FileReader(filename);
       } catch (FileNotFoundException e) {
           e.printStackTrace();
       }
       Iterable<CSVRecord> records = null;
       try {
           records = CSVFormat.DEFAULT.parse(in);
       } catch (IOException e) {
           e.printStackTrace();
       }
       for (CSVRecord record : records) {
    	   
    	   if(filename.contains("PURL")) {
    		   actualHeaders = new String[record.size()-1];
    	   }
    	   else {
    		   actualHeaders = new String[record.size()]; 
    	   }
           
           for (int i = 0; i < actualHeaders.length; i++){
               actualHeaders[i] = record.get(i);
               // if expected column versus actual column is NOT equal, then set boolean flag as false
               if (!HEADERS[i].equals(actualHeaders[i])) {
                   isOrderOfHeadersCorrectInCsvFile = false;
               }
           }
           // Exit the loop after first iteration, since we want to validate just headers.
           break;
       }

//       System.out.println("expectedHeaders: " + iterateStringArray(HEADERS));
//       System.out.println("actualHeaders: " + iterateStringArray(actualHeaders));
       System.out.println("isOrderOfHeadersCorrectInCsvFile: " +filename+ " - "+ isOrderOfHeadersCorrectInCsvFile);

       // if Order Of Headers Is NOT Correct In Csv File, then provide error details.
       if (!isOrderOfHeadersCorrectInCsvFile) {
           String invalidOrderOfHeadersMessage = "Mismatch in 'Expected' versus 'Actual' fields/columns/headers in csv file." +
                   " Expected headers " + iterateStringArray(HEADERS) + ". But Got Actual headers " + iterateStringArray(actualHeaders);
           errorLinkedHashMap.put(1, invalidOrderOfHeadersMessage);
       }

       return isOrderOfHeadersCorrectInCsvFile;
   }
   public static String iterateStringArray(String[] array) {
       String output = "[";
       for (int i = 0; i < array.length; i++) {
           if (i == array.length - 1) {
               output += array[i];
           }
           else {
               output += array[i] + ", ";
           }
       }
       output += "]";
       return output;
   }
   public static void validateFileContents_ifOrdersMatched(String filename) throws FileNotFoundException
   {
	   boolean file1Order = isOrderOfHeadersCorrectInCsvFile(filename);
	   if(file1Order)
	   {
		   List<String> filecontents1 = readFile(filename); 
	   }
   	
   }
}

	
			