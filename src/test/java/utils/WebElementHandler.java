//package utils;
//
//import org.junit.Assert;
//import org.openqa.selenium.JavascriptExecutor;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;
//
//public class WebElementHandler {
//
//    public static void waitForPageLoad(WebDriver driver, int timeOutInSeconds) {
//        try {
//            WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
//            wait.until(webDriver -> ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete"));
//        } catch (Throwable error) {
//            Assert.fail("Timeout waiting for Page Load Request to complete.");
//        }
//    }
//
//    public static boolean isWebElementDisplayed(WebDriver driver, WebElement webElement) {
//        boolean isWebElementDisplayed = false;
//        try {
//            WebDriverWait wait = new WebDriverWait(driver, 30);
//            wait.until(ExpectedConditions.visibilityOf(webElement));
//            isWebElementDisplayed = true;
//        }
//        catch (Exception e) {
//            System.out.println(webElement + " is not displayed.");
//            System.out.println(e);
//        }
//        return isWebElementDisplayed;
//    }
//
//    public static void clickWebElement(WebDriver driver, WebElement webElement) {
//        try {
//            WebDriverWait wait = new WebDriverWait(driver, 30);
//            wait.until(ExpectedConditions.visibilityOf(webElement));
//            webElement.click();
//        }
//        catch (Exception e) {
//            System.out.println(webElement + " could not be clicked.");
//            System.out.println(e);
//        }
//    }
//}
